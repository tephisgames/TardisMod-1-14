package net.tardis.mod.commands;

import com.mojang.brigadier.CommandDispatcher;

import net.minecraft.command.CommandSource;
import net.minecraft.command.Commands;
import net.minecraft.command.arguments.ArgumentSerializer;
import net.minecraft.command.arguments.ArgumentTypes;
import net.tardis.mod.Tardis;
import net.tardis.mod.commands.argument.ConsoleRoomArgument;
import net.tardis.mod.commands.argument.ExteriorArgument;
import net.tardis.mod.commands.subcommands.AttuneItemCommand;
import net.tardis.mod.commands.subcommands.LoyaltyCommand;
import net.tardis.mod.commands.subcommands.RefuelCommand;
import net.tardis.mod.commands.subcommands.SetupCommand;
import net.tardis.mod.commands.subcommands.TardisNameCommand;
import net.tardis.mod.commands.subcommands.TraitsCommand;
import net.tardis.mod.commands.subcommands.UnlockCommand;
import net.tardis.mod.helper.Helper;


public class TardisCommand {
	
    public static void register(CommandDispatcher<CommandSource> dispatcher){
        dispatcher.register(
                Commands.literal(Tardis.MODID)
                        .then(SetupCommand.register(dispatcher))
                        .then(UnlockCommand.register(dispatcher))
                        .then(TardisNameCommand.register(dispatcher))
                        .then(TraitsCommand.register(dispatcher))
                        .then(LoyaltyCommand.register(dispatcher))
                        .then(AttuneItemCommand.register(dispatcher))
                        .then(RefuelCommand.register(dispatcher))
        );
    }
    
    /** Register our custom Argument Types to allow for proper serialisation on the server
     * <br> Do this in FMLCommonSetup, enqueueWork lambda*/
    public static void registerCustomArgumentTypes() {
    	ArgumentTypes.register(Helper.createRLString("exterior_argument"), ExteriorArgument.class, new ArgumentSerializer<>(ExteriorArgument::getExteriorArgument));
    	ArgumentTypes.register(Helper.createRLString("console_room_argument"), ConsoleRoomArgument.class, new ArgumentSerializer<>(ConsoleRoomArgument::getConsoleRoomArgument));
    }
    
    
}
