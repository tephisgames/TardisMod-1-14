package net.tardis.mod.sounds;

import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;

/**
 * Sound Registry
 * The Registry name MUST be the same in the SOUNDS#register and setUpSound method
 *
 */
public class TSounds {

	public static final DeferredRegister<SoundEvent> SOUNDS = DeferredRegister.create(ForgeRegistries.SOUND_EVENTS, Tardis.MODID);
    //Tardis Land/Takeoff
	public static final RegistryObject<SoundEvent> TARDIS_TAKEOFF = SOUNDS.register("tardis_takeoff", () -> setupSound("tardis_takeoff"));
	public static final RegistryObject<SoundEvent> TARDIS_LAND = SOUNDS.register("tardis_land", () -> setupSound("tardis_land"));
	public static final RegistryObject<SoundEvent> TARDIS_FLY_LOOP = SOUNDS.register("tardis_fly_loop", () -> setupSound("tardis_fly_loop"));
	public static final RegistryObject<SoundEvent> ROTOR_START = SOUNDS.register("rotor_start", () -> setupSound("rotor_start"));
	public static final RegistryObject<SoundEvent> ROTOR_TICK = SOUNDS.register("rotor_tick", () -> setupSound("rotor_tick"));
	public static final RegistryObject<SoundEvent> ROTOR_END = SOUNDS.register("rotor_end", () -> setupSound("rotor_end"));
	public static final RegistryObject<SoundEvent> POWER_DOWN = SOUNDS.register("power_down", () -> setupSound("power_down"));
	public static final RegistryObject<SoundEvent> SUBSYSTEMS_OFF = SOUNDS.register("subsystems_off", () -> setupSound("subsystems_off"));
	public static final RegistryObject<SoundEvent> SUBSYSTEMS_ON = SOUNDS.register("subsystems_on", () -> setupSound("subsystems_on"));
	public static final RegistryObject<SoundEvent> TARDIS_FIRST_ENTRANCE = SOUNDS.register("tardis_first_entrance", () -> setupSound("tardis_first_entrance"));
	public static final RegistryObject<SoundEvent> TARDIS_SHUT_DOWN = SOUNDS.register("tardis_shut_down", () -> setupSound("tardis_shut_down"));
	public static final RegistryObject<SoundEvent> TARDIS_POWER_UP = SOUNDS.register("tardis_power_up", () -> setupSound("tardis_power_up"));
	public static final RegistryObject<SoundEvent> TARDIS_LAND_NOTIFICATION = SOUNDS.register("tardis_land_notification", () -> setupSound("tardis_land_notification"));
	//Exterior
	public static final RegistryObject<SoundEvent> DOOR_OPEN = SOUNDS.register("door_open", () -> setupSound("door_open"));
	public static final RegistryObject<SoundEvent> DOOR_CLOSE = SOUNDS.register("door_close", () -> setupSound("door_close"));
	public static final RegistryObject<SoundEvent> DOOR_LOCK = SOUNDS.register("door_lock", () -> setupSound("door_lock"));
	public static final RegistryObject<SoundEvent> DOOR_UNLOCK = SOUNDS.register("door_unlock", () -> setupSound("door_unlock"));
	public static final RegistryObject<SoundEvent> DOOR_KNOCK = SOUNDS.register("door_knock", () -> setupSound("door_knock"));
	public static final RegistryObject<SoundEvent> CAR_LOCK = SOUNDS.register("car_lock", () -> setupSound("car_lock"));
	public static final RegistryObject<SoundEvent> WOOD_DOOR_OPEN = SOUNDS.register("wood_door_open", () -> setupSound("wood_door_open"));
	public static final RegistryObject<SoundEvent> WOOD_DOOR_CLOSE = SOUNDS.register("wood_door_close", () -> setupSound("wood_door_close"));
    //Errors
	public static final RegistryObject<SoundEvent> SINGLE_CLOISTER = SOUNDS.register("single_cloister", () -> setupSound("single_cloister"));
	public static final RegistryObject<SoundEvent> ALARM_LOW = SOUNDS.register("alarm_low", () -> setupSound("alarm_low"));
	public static final RegistryObject<SoundEvent> CANT_START = SOUNDS.register("cant_start", () -> setupSound("cant_start"));
    //Console Sounds
	public static final RegistryObject<SoundEvent> HANDBRAKE_ENGAGE = SOUNDS.register("handbrake_engage", () -> setupSound("handbrake_engage"));
	public static final RegistryObject<SoundEvent> HANDBRAKE_RELEASE = SOUNDS.register("handbrake_release", () -> setupSound("handbrake_release"));
	public static final RegistryObject<SoundEvent> THROTTLE = SOUNDS.register("throttle", () -> setupSound("throttle"));
	public static final RegistryObject<SoundEvent> RANDOMISER = SOUNDS.register("randomiser", () -> setupSound("randomiser"));
	public static final RegistryObject<SoundEvent> GENERIC_ONE = SOUNDS.register("generic_one", () -> setupSound("generic_one"));
	public static final RegistryObject<SoundEvent> GENERIC_TWO = SOUNDS.register("generic_two", () -> setupSound("generic_two"));
	public static final RegistryObject<SoundEvent> GENERIC_THREE = SOUNDS.register("generic_three", () -> setupSound("generic_three"));
	public static final RegistryObject<SoundEvent> DIMENSION = SOUNDS.register("dimension", () -> setupSound("dimension"));
	public static final RegistryObject<SoundEvent> DIRECTION = SOUNDS.register("direction", () -> setupSound("direction"));
	public static final RegistryObject<SoundEvent> LANDING_TYPE_UP = SOUNDS.register("landing_type_up", () -> setupSound("landing_type_up"));
	public static final RegistryObject<SoundEvent> LANDING_TYPE_DOWN = SOUNDS.register("landing_type_down", () -> setupSound("landing_type_down"));
	public static final RegistryObject<SoundEvent> REFUEL_START = SOUNDS.register("refuel_start", () -> setupSound("refuel_start"));
	public static final RegistryObject<SoundEvent> REFUEL_STOP = SOUNDS.register("refuel_stop", () -> setupSound("refuel_stop"));

	public static final RegistryObject<SoundEvent> COMMUNICATOR_BEEP = SOUNDS.register("communicator_beep", () -> setupSound("communicator_beep"));
	public static final RegistryObject<SoundEvent> COMMUNICATOR_RING = SOUNDS.register("communicator_ring", () -> setupSound("communicator_ring"));
	public static final RegistryObject<SoundEvent> COMMUNICATOR_PHONE_PICKUP = SOUNDS.register("communicator_phone_pickup", () -> setupSound("communicator_phone_pickup"));
	public static final RegistryObject<SoundEvent> COMMUNICATOR_STEAM = SOUNDS.register("communicator_steam", () -> setupSound("communicator_steam"));
	public static final RegistryObject<SoundEvent> STABILIZER_ON = SOUNDS.register("stabilizer_on", () -> setupSound("stabilizer_on"));
	public static final RegistryObject<SoundEvent> STABILIZER_OFF = SOUNDS.register("stabilizer_off", () -> setupSound("stabilizer_off"));
	public static final RegistryObject<SoundEvent> TELEPATHIC_CIRCUIT = SOUNDS.register("telepathic_circuit", () -> setupSound("telepathic_circuit"));
	public static final RegistryObject<SoundEvent> SCREEN_BEEP_SINGLE = SOUNDS.register("screen_beep_single", () -> setupSound("screen_beep_single"));
    //Sonics
	public static final RegistryObject<SoundEvent> SONIC_GENERIC = SOUNDS.register("sonic_generic", () -> setupSound("sonic_generic"));
	public static final RegistryObject<SoundEvent> SONIC_TUNING = SOUNDS.register("sonic_tuning", () -> setupSound("sonic_tuning"));
	public static final RegistryObject<SoundEvent> SONIC_FAIL = SOUNDS.register("sonic_fail", () -> setupSound("sonic_fail"));
	public static final RegistryObject<SoundEvent> SONIC_BROKEN = SOUNDS.register("sonic_broken", () -> setupSound("sonic_broken"));
    //Sound Schemes
	public static final RegistryObject<SoundEvent> TAKEOFF_TV = SOUNDS.register("takeoff_tv", () -> setupSound("takeoff_tv"));
	public static final RegistryObject<SoundEvent> LAND_TV = SOUNDS.register("land_tv", () -> setupSound("land_tv"));
	public static final RegistryObject<SoundEvent> FLY_LOOP_TV = SOUNDS.register("fly_loop_tv", () -> setupSound("fly_loop_tv"));
	public static final RegistryObject<SoundEvent> MASTER_TAKEOFF = SOUNDS.register("master_takeoff", () -> setupSound("master_takeoff"));
	public static final RegistryObject<SoundEvent> MASTER_LAND = SOUNDS.register("master_land", () -> setupSound("master_land"));
	public static final RegistryObject<SoundEvent> JUNK_TAKEOFF = SOUNDS.register("junk_takeoff", () -> setupSound("junk_takeoff"));
	public static final RegistryObject<SoundEvent> JUNK_LAND = SOUNDS.register("junk_land", () -> setupSound("junk_land"));
	
	//Vortex Manipulator
	public static final RegistryObject<SoundEvent> VM_TELEPORT = SOUNDS.register("vm_teleport", () -> setupSound("vm_teleport"));
	public static final RegistryObject<SoundEvent> VM_BUTTON = SOUNDS.register("vm_button", () -> setupSound("vm_button"));
	public static final RegistryObject<SoundEvent> VM_TELEPORT_DEST = SOUNDS.register("vm_teleport_dest", () -> setupSound("vm_teleport_dest"));
    //Misc
	public static final RegistryObject<SoundEvent> REMOTE_ACCEPT = SOUNDS.register("remote_accept", () -> setupSound("remote_accept"));

	public static final RegistryObject<SoundEvent> PAPER_DROP = SOUNDS.register("paper_drop", () -> setupSound("paper_drop"));
    //Dalekas
	public static final RegistryObject<SoundEvent> DALEK_EXTERMINATE = SOUNDS.register("dalek_exterminate", () -> setupSound("dalek_exterminate"));
	public static final RegistryObject<SoundEvent> DALEK_FIRE = SOUNDS.register("dalek_fire", () -> setupSound("dalek_fire"));
	public static final RegistryObject<SoundEvent> DALEK_HOVER = SOUNDS.register("dalek_hover", () -> setupSound("dalek_hover"));
	public static final RegistryObject<SoundEvent> DALEK_DEATH = SOUNDS.register("dalek_death", () -> setupSound("dalek_death"));
	public static final RegistryObject<SoundEvent> DALEK_MOVES = SOUNDS.register("dalek_moves", () -> setupSound("dalek_moves"));
	public static final RegistryObject<SoundEvent> DALEK_ARM = SOUNDS.register("dalek_arm", () -> setupSound("dalek_arm"));

	public static final RegistryObject<SoundEvent> DALEK_SW_FIRE =SOUNDS.register("dalek_sw_fire", () -> setupSound("dalek_sw_fire"));
	public static final RegistryObject<SoundEvent> DALEK_SW_AIM = SOUNDS.register("dalek_sw_aim", () -> setupSound("dalek_sw_aim"));
	public static final RegistryObject<SoundEvent> DALEK_SW_HIT_EXPLODE = SOUNDS.register("dalek_sw_hit_explode", () -> setupSound("dalek_sw_hit_explode"));
    //Bessie
	public static final RegistryObject<SoundEvent> BESSIE_DRIVE = SOUNDS.register("bessie_drive", () -> setupSound("bessie_drive"));
	public static final RegistryObject<SoundEvent> BESSIE_HORN = SOUNDS.register("bessie_horn", () -> setupSound("bessie_horn"));
    //Hums
	public static final RegistryObject<SoundEvent> SHIELD_HUM = SOUNDS.register("shield_hum", () -> setupSound("shield_hum"));

	public static final RegistryObject<SoundEvent> TARDIS_HUM_63 = SOUNDS.register("tardis_hum_63", () -> setupSound("tardis_hum_63"));
	public static final RegistryObject<SoundEvent> TARDIS_HUM_70 = SOUNDS.register("tardis_hum_70", () -> setupSound("tardis_hum_70"));
	public static final RegistryObject<SoundEvent> TARDIS_HUM_80 = SOUNDS.register("tardis_hum_80", () -> setupSound("tardis_hum_80"));
	public static final RegistryObject<SoundEvent> TARDIS_HUM_COPPER = SOUNDS.register("tardis_hum_copper", () -> setupSound("tardis_hum_copper"));
	public static final RegistryObject<SoundEvent> TARDIS_HUM_CORAL = SOUNDS.register("tardis_hum_coral", () -> setupSound("tardis_hum_coral"));
	public static final RegistryObject<SoundEvent> TARDIS_HUM_TOYOTA = SOUNDS.register("tardis_hum_toyota", () -> setupSound("tardis_hum_toyota"));
    //Ambient sounds
	public static final RegistryObject<SoundEvent> AMBIENT_CREAKS = SOUNDS.register("ambient_creaks", () -> setupSound("ambient_creaks"));
	public static final RegistryObject<SoundEvent> ELECTRIC_SPARK = SOUNDS.register("electric_spark", () -> setupSound("electric_spark"));
	public static final RegistryObject<SoundEvent> ELECTRIC_ARC = SOUNDS.register("electric_arc", () -> setupSound("electric_arc"));
	public static final RegistryObject<SoundEvent> STEAM_HISS = SOUNDS.register("steam_hiss", () -> setupSound("steam_hiss"));
	public static final RegistryObject<SoundEvent> REACHED_DESTINATION = SOUNDS.register("reached_destination", () -> setupSound("reached_destination"));
    //Monitor
	public static final RegistryObject<SoundEvent> EYE_MONITOR_INTERACT = SOUNDS.register("eye_monitor_interact", () -> setupSound("eye_monitor_interact"));
	public static final RegistryObject<SoundEvent> STEAMPUNK_MONITOR_INTERACT = SOUNDS.register("steampunk_monitor_interact", () -> setupSound("steampunk_monitor_interact"));
    //Items
	public static final RegistryObject<SoundEvent> WATCH_MALFUNCTION = SOUNDS.register("watch_malfunction", () -> setupSound("watch_malfunction"));
	public static final RegistryObject<SoundEvent> WATCH_TICK = SOUNDS.register("watch_tick", () -> setupSound("watch_tick"));
	public static final RegistryObject<SoundEvent> LASER_GUN_FIRE = SOUNDS.register("laser_gun_fire", () -> setupSound("laser_gun_fire"));
    //Keybind sounds
	public static final RegistryObject<SoundEvent> SNAP = SOUNDS.register("snap", () -> setupSound("snap"));

	private static SoundEvent setupSound(String soundName) {
        return new SoundEvent(new ResourceLocation(Tardis.MODID, soundName));
    }
    
}
