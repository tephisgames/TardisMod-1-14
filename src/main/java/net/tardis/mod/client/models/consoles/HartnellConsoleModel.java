package net.tardis.mod.client.models.consoles;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.MathHelper;
import net.tardis.mod.client.models.LightModelRenderer;
import net.tardis.mod.client.models.TileModel;
import net.tardis.mod.controls.DoorControl;
import net.tardis.mod.controls.FacingControl;
import net.tardis.mod.controls.HandbrakeControl;
import net.tardis.mod.controls.IncModControl;
import net.tardis.mod.controls.LandingTypeControl;
import net.tardis.mod.controls.LandingTypeControl.EnumLandType;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.controls.RandomiserControl;
import net.tardis.mod.controls.StabilizerControl;
import net.tardis.mod.controls.ThrottleControl;
import net.tardis.mod.controls.XControl;
import net.tardis.mod.controls.YControl;
import net.tardis.mod.controls.ZControl;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.subsystem.StabilizerSubsystem;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;

public class HartnellConsoleModel extends EntityModel<Entity> implements TileModel<HartnellConsoleTile> {
	private final ModelRenderer base_console;
	private final ModelRenderer plinth;
	private final ModelRenderer bone;
	private final ModelRenderer bone2;
	private final ModelRenderer bone3;
	private final ModelRenderer bone4;
	private final ModelRenderer bone5;
	private final ModelRenderer bone6;
	private final ModelRenderer bone79;
	private final ModelRenderer bone85;
	private final ModelRenderer bone80;
	private final ModelRenderer bone81;
	private final ModelRenderer bone82;
	private final ModelRenderer bone83;
	private final ModelRenderer bone84;
	private final ModelRenderer bone86;
	private final ModelRenderer bone87;
	private final ModelRenderer bone88;
	private final ModelRenderer bone89;
	private final ModelRenderer bone90;
	private final ModelRenderer bone13;
	private final ModelRenderer bone19;
	private final ModelRenderer bone21;
	private final ModelRenderer bone22;
	private final ModelRenderer bone23;
	private final ModelRenderer bone24;
	private final ModelRenderer bone7;
	private final ModelRenderer bone8;
	private final ModelRenderer bone9;
	private final ModelRenderer bone10;
	private final ModelRenderer bone11;
	private final ModelRenderer bone12;
	private final ModelRenderer bone14;
	private final ModelRenderer bone15;
	private final ModelRenderer bone16;
	private final ModelRenderer bone17;
	private final ModelRenderer bone18;
	private final ModelRenderer bone20;
	private final ModelRenderer bone43;
	private final ModelRenderer bone49;
	private final ModelRenderer bone44;
	private final ModelRenderer bone45;
	private final ModelRenderer bone46;
	private final ModelRenderer bone47;
	private final ModelRenderer bone48;
	private final ModelRenderer bone50;
	private final ModelRenderer bone51;
	private final ModelRenderer bone52;
	private final ModelRenderer bone53;
	private final ModelRenderer bone54;
	private final ModelRenderer bone67;
	private final ModelRenderer bone68;
	private final ModelRenderer bone69;
	private final ModelRenderer bone70;
	private final ModelRenderer bone71;
	private final ModelRenderer bone72;
	private final ModelRenderer bone73;
	private final ModelRenderer bone74;
	private final ModelRenderer bone75;
	private final ModelRenderer bone76;
	private final ModelRenderer bone77;
	private final ModelRenderer bone78;
	private final ModelRenderer bone125;
	private final ModelRenderer bone126;
	private final ModelRenderer bone127;
	private final ModelRenderer bone128;
	private final ModelRenderer bone129;
	private final ModelRenderer bone130;
	private final ModelRenderer bone148;
	private final ModelRenderer bone149;
	private final ModelRenderer bone150;
	private final ModelRenderer bone151;
	private final ModelRenderer bone152;
	private final ModelRenderer bone153;
	private final ModelRenderer bone25;
	private final ModelRenderer bone26;
	private final ModelRenderer bone27;
	private final ModelRenderer bone28;
	private final ModelRenderer bone29;
	private final ModelRenderer bone30;
	private final ModelRenderer bone61;
	private final ModelRenderer bone62;
	private final ModelRenderer bone63;
	private final ModelRenderer bone64;
	private final ModelRenderer bone65;
	private final ModelRenderer bone66;
	private final ModelRenderer bone55;
	private final ModelRenderer bone56;
	private final ModelRenderer bone57;
	private final ModelRenderer bone58;
	private final ModelRenderer bone59;
	private final ModelRenderer bone60;
	private final ModelRenderer bone37;
	private final ModelRenderer bone38;
	private final ModelRenderer bone39;
	private final ModelRenderer bone40;
	private final ModelRenderer bone41;
	private final ModelRenderer bone42;
	private final ModelRenderer bone31;
	private final ModelRenderer bone32;
	private final ModelRenderer bone33;
	private final ModelRenderer bone34;
	private final ModelRenderer bone35;
	private final ModelRenderer bone36;
	private final ModelRenderer components;
	private final ModelRenderer north_panel;
	private final ModelRenderer bone110;
	private final LightModelRenderer glow_button11;
	private final LightModelRenderer glow_button9;
	private final LightModelRenderer glow_button6;
	private final LightModelRenderer glow_button10;
	private final LightModelRenderer glow_button7;
	private final LightModelRenderer glow_button8;
	private final ModelRenderer button3_rotate_X;
	private final ModelRenderer rotate1_Y27;
	private final ModelRenderer button2_rotate_X;
	private final ModelRenderer doorcontrol2_rotate_x45;
	private final ModelRenderer doorcontrol1_rotate_X45;
	private final ModelRenderer north_east_panel;
	private final ModelRenderer bone109;
	private final LightModelRenderer glow_button13;
	private final LightModelRenderer glow_button12;
	private final LightModelRenderer glow_button14;
	private final ModelRenderer button8_rotate_X;
	private final ModelRenderer button4_rotate_X;
	private final ModelRenderer button6_rotate_X;
	private final ModelRenderer button5_rotate_X;
	private final ModelRenderer sonicport;
	private final ModelRenderer rockerswitch5_rotate_X22;
	private final ModelRenderer bone111;
	private final ModelRenderer south_east_panel;
	private final ModelRenderer bone112;
	private final ModelRenderer switch3_rotate_X22;
	private final ModelRenderer switch4_rotate_22;
	private final LightModelRenderer glow_button18;
	private final LightModelRenderer glow_button20;
	private final LightModelRenderer glow_button17;
	private final LightModelRenderer glow_button16;
	private final LightModelRenderer glow_button19;
	private final LightModelRenderer glow_button15;
	private final ModelRenderer biglever_rotate_X80;
	private final ModelRenderer directioncontrol_rotate_Y50;
	private final ModelRenderer landtype_rotate_Y50;
	private final ModelRenderer south_panel;
	private final ModelRenderer bone113;
	private final ModelRenderer fastreturn_nomovement;
	private final LightModelRenderer glow_button23;
	private final LightModelRenderer glow_button22;
	private final LightModelRenderer glow_button21;
	private final LightModelRenderer glow_button25;
	private final LightModelRenderer glow_button24;
	private final ModelRenderer throttle_rotate_X100;
	private final ModelRenderer handbreak_rotate_X2;
	private final ModelRenderer button7_rotate_X;
	private final ModelRenderer rockerswitch7_rotate_X22;
	private final ModelRenderer rockerswitch6_rotate_X22;
	private final ModelRenderer stabiliserspart1_rotate_X70;
	private final ModelRenderer stabiliserspart2_rotate_X70;
	private final ModelRenderer south_west_panel;
	private final ModelRenderer bone114;
	private final ModelRenderer button1_rotate_X;
	private final ModelRenderer rotate_Y27;
	private final LightModelRenderer x_increment;
	private final LightModelRenderer Y_increment;
	private final LightModelRenderer Z_increment;
	private final ModelRenderer spin7_rotate_Y55;
	private final ModelRenderer spin6_rotate_aY55;
	private final ModelRenderer spin5_rotate_Y55;
	private final ModelRenderer button_rotate_X;
	private final LightModelRenderer glow_button;
	private final ModelRenderer rockerswitch4_rotate_X22;
	private final ModelRenderer rockerswitch2_rotate_X22;
	private final ModelRenderer rockerswitch3_rotate_X22;
	private final ModelRenderer increment_rotate_X60;
	private final ModelRenderer north_west_panel;
	private final ModelRenderer bone118;
	private final ModelRenderer randomiser_rotate_X;
	private final LightModelRenderer glow_button3;
	private final LightModelRenderer glow_button4;
	private final LightModelRenderer glow_button5;
	private final LightModelRenderer glow_button2;
	private final ModelRenderer switch_rotate_35;
	private final ModelRenderer rockerswitch1_rotate_22;
	private final ModelRenderer refuel1_rotate_45;
	private final ModelRenderer refuel2_rotate_45;
	private final ModelRenderer refuel3_rotate_45;
	private final ModelRenderer lever4_rotate_45;
	private final ModelRenderer spin3_rotate_55;
	private final ModelRenderer spin4_rotate_55;
	private final ModelRenderer spin_rotate_55;
	private final ModelRenderer spin2_rotate_55;
	private final ModelRenderer rotor_translate_8;
	private final ModelRenderer bone94;
	private final ModelRenderer inner_rotor_rotate_y;
	private final ModelRenderer bone95;
	private final ModelRenderer bone134;
	private final ModelRenderer bone135;
	private final ModelRenderer bone136;
	private final ModelRenderer bone143;
	private final ModelRenderer bone144;
	private final ModelRenderer bone145;

	public HartnellConsoleModel() {
		textureWidth = 128;
		textureHeight = 128;

		base_console = new ModelRenderer(this);
		base_console.setRotationPoint(0.0F, 22.0F, 0.0F);
		

		plinth = new ModelRenderer(this);
		plinth.setRotationPoint(0.0F, 2.0F, 0.0F);
		base_console.addChild(plinth);
		

		bone = new ModelRenderer(this);
		bone.setRotationPoint(0.0F, 0.0F, 0.0F);
		plinth.addChild(bone);
		bone.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone2 = new ModelRenderer(this);
		bone2.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone.addChild(bone2);
		setRotationAngle(bone2, 0.0F, -1.0472F, 0.0F);
		bone2.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone3 = new ModelRenderer(this);
		bone3.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone2.addChild(bone3);
		setRotationAngle(bone3, 0.0F, -1.0472F, 0.0F);
		bone3.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone4 = new ModelRenderer(this);
		bone4.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone3.addChild(bone4);
		setRotationAngle(bone4, 0.0F, -1.0472F, 0.0F);
		bone4.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone5 = new ModelRenderer(this);
		bone5.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone4.addChild(bone5);
		setRotationAngle(bone5, 0.0F, -1.0472F, 0.0F);
		bone5.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone6 = new ModelRenderer(this);
		bone6.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone5.addChild(bone6);
		setRotationAngle(bone6, 0.0F, -1.0472F, 0.0F);
		bone6.setTextureOffset(48, 43).addBox(-7.0F, -2.0F, 12.13F, 14.0F, 2.0F, 2.0F, 0.0F, false);

		bone79 = new ModelRenderer(this);
		bone79.setRotationPoint(0.0F, 0.0F, 0.0F);
		plinth.addChild(bone79);
		

		bone85 = new ModelRenderer(this);
		bone85.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone79.addChild(bone85);
		setRotationAngle(bone85, -0.7854F, 0.0F, 0.0F);
		bone85.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone80 = new ModelRenderer(this);
		bone80.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone79.addChild(bone80);
		setRotationAngle(bone80, 0.0F, -1.0472F, 0.0F);
		

		bone81 = new ModelRenderer(this);
		bone81.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone80.addChild(bone81);
		setRotationAngle(bone81, -0.7854F, 0.0F, 0.0F);
		bone81.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone82 = new ModelRenderer(this);
		bone82.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone80.addChild(bone82);
		setRotationAngle(bone82, 0.0F, -1.0472F, 0.0F);
		

		bone83 = new ModelRenderer(this);
		bone83.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone82.addChild(bone83);
		setRotationAngle(bone83, -0.7854F, 0.0F, 0.0F);
		bone83.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone84 = new ModelRenderer(this);
		bone84.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone82.addChild(bone84);
		setRotationAngle(bone84, 0.0F, -1.0472F, 0.0F);
		

		bone86 = new ModelRenderer(this);
		bone86.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone84.addChild(bone86);
		setRotationAngle(bone86, -0.7854F, 0.0F, 0.0F);
		bone86.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone87 = new ModelRenderer(this);
		bone87.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone84.addChild(bone87);
		setRotationAngle(bone87, 0.0F, -1.0472F, 0.0F);
		

		bone88 = new ModelRenderer(this);
		bone88.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone87.addChild(bone88);
		setRotationAngle(bone88, -0.7854F, 0.0F, 0.0F);
		bone88.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone89 = new ModelRenderer(this);
		bone89.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone87.addChild(bone89);
		setRotationAngle(bone89, 0.0F, -1.0472F, 0.0F);
		

		bone90 = new ModelRenderer(this);
		bone90.setRotationPoint(0.0F, -2.0F, 14.13F);
		bone89.addChild(bone90);
		setRotationAngle(bone90, -0.7854F, 0.0F, 0.0F);
		bone90.setTextureOffset(76, 21).addBox(-7.0F, 0.0F, -6.0F, 14.0F, 2.0F, 6.0F, 0.0F, false);

		bone13 = new ModelRenderer(this);
		bone13.setRotationPoint(0.0F, 0.0F, 0.0F);
		plinth.addChild(bone13);
		bone13.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone19 = new ModelRenderer(this);
		bone19.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone13.addChild(bone19);
		setRotationAngle(bone19, 0.0F, -1.0472F, 0.0F);
		bone19.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone21 = new ModelRenderer(this);
		bone21.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone19.addChild(bone21);
		setRotationAngle(bone21, 0.0F, -1.0472F, 0.0F);
		bone21.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone22 = new ModelRenderer(this);
		bone22.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone21.addChild(bone22);
		setRotationAngle(bone22, 0.0F, -1.0472F, 0.0F);
		bone22.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone23 = new ModelRenderer(this);
		bone23.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone22.addChild(bone23);
		setRotationAngle(bone23, 0.0F, -1.0472F, 0.0F);
		bone23.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone24 = new ModelRenderer(this);
		bone24.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone23.addChild(bone24);
		setRotationAngle(bone24, 0.0F, -1.0472F, 0.0F);
		bone24.setTextureOffset(68, 43).addBox(-7.0F, -1.0F, -0.87F, 14.0F, 1.0F, 13.0F, 0.0F, false);

		bone7 = new ModelRenderer(this);
		bone7.setRotationPoint(0.0F, 0.0F, 0.0F);
		plinth.addChild(bone7);
		setRotationAngle(bone7, 0.0F, -0.5236F, 0.0F);
		bone7.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone8 = new ModelRenderer(this);
		bone8.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone7.addChild(bone8);
		setRotationAngle(bone8, 0.0F, -1.0472F, 0.0F);
		bone8.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone9 = new ModelRenderer(this);
		bone9.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone8.addChild(bone9);
		setRotationAngle(bone9, 0.0F, -1.0472F, 0.0F);
		bone9.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone10 = new ModelRenderer(this);
		bone10.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone9.addChild(bone10);
		setRotationAngle(bone10, 0.0F, -1.0472F, 0.0F);
		bone10.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone11 = new ModelRenderer(this);
		bone11.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone10.addChild(bone11);
		setRotationAngle(bone11, 0.0F, -1.0472F, 0.0F);
		bone11.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone12 = new ModelRenderer(this);
		bone12.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone11.addChild(bone12);
		setRotationAngle(bone12, 0.0F, -1.0472F, 0.0F);
		bone12.setTextureOffset(0, 89).addBox(-1.0F, -26.0F, 11.74F, 2.0F, 26.0F, 4.0F, 0.0F, false);

		bone14 = new ModelRenderer(this);
		bone14.setRotationPoint(0.0F, -2.0F, 0.0F);
		plinth.addChild(bone14);
		bone14.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone15 = new ModelRenderer(this);
		bone15.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone14.addChild(bone15);
		setRotationAngle(bone15, 0.0F, -1.0472F, 0.0F);
		bone15.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone16 = new ModelRenderer(this);
		bone16.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone15.addChild(bone16);
		setRotationAngle(bone16, 0.0F, -1.0472F, 0.0F);
		bone16.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone17 = new ModelRenderer(this);
		bone17.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone16.addChild(bone17);
		setRotationAngle(bone17, 0.0F, -1.0472F, 0.0F);
		bone17.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone18 = new ModelRenderer(this);
		bone18.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone17.addChild(bone18);
		setRotationAngle(bone18, 0.0F, -1.0472F, 0.0F);
		bone18.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone20 = new ModelRenderer(this);
		bone20.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone18.addChild(bone20);
		setRotationAngle(bone20, 0.0F, -1.0472F, 0.0F);
		bone20.setTextureOffset(40, 75).addBox(-6.0F, -24.0F, 8.86F, 12.0F, 24.0F, 2.0F, 0.0F, false);

		bone43 = new ModelRenderer(this);
		bone43.setRotationPoint(0.0F, -8.0F, 0.0F);
		plinth.addChild(bone43);
		setRotationAngle(bone43, 0.0F, -0.5236F, 0.0F);
		

		bone49 = new ModelRenderer(this);
		bone49.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone43.addChild(bone49);
		setRotationAngle(bone49, 0.2618F, 0.0F, 0.0F);
		bone49.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone44 = new ModelRenderer(this);
		bone44.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone43.addChild(bone44);
		setRotationAngle(bone44, 0.0F, -1.0472F, 0.0F);
		

		bone45 = new ModelRenderer(this);
		bone45.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone44.addChild(bone45);
		setRotationAngle(bone45, 0.3054F, 0.0F, 0.0F);
		bone45.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone46 = new ModelRenderer(this);
		bone46.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone44.addChild(bone46);
		setRotationAngle(bone46, 0.0F, -1.0472F, 0.0F);
		

		bone47 = new ModelRenderer(this);
		bone47.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone46.addChild(bone47);
		setRotationAngle(bone47, 0.2618F, 0.0F, 0.0F);
		bone47.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone48 = new ModelRenderer(this);
		bone48.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone46.addChild(bone48);
		setRotationAngle(bone48, 0.0F, -1.0472F, 0.0F);
		

		bone50 = new ModelRenderer(this);
		bone50.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone48.addChild(bone50);
		setRotationAngle(bone50, 0.3054F, 0.0F, 0.0F);
		bone50.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone51 = new ModelRenderer(this);
		bone51.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone48.addChild(bone51);
		setRotationAngle(bone51, 0.0F, -1.0472F, 0.0F);
		

		bone52 = new ModelRenderer(this);
		bone52.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone51.addChild(bone52);
		setRotationAngle(bone52, 0.2618F, 0.0F, 0.0F);
		bone52.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone53 = new ModelRenderer(this);
		bone53.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone51.addChild(bone53);
		setRotationAngle(bone53, 0.0F, -1.0472F, 0.0F);
		

		bone54 = new ModelRenderer(this);
		bone54.setRotationPoint(0.0F, -18.0F, 31.73F);
		bone53.addChild(bone54);
		setRotationAngle(bone54, 0.2618F, 0.0F, 0.0F);
		bone54.setTextureOffset(72, 72).addBox(-1.0F, -6.0F, -18.0F, 2.0F, 6.0F, 18.0F, 0.0F, false);

		bone67 = new ModelRenderer(this);
		bone67.setRotationPoint(0.0F, -44.0F, 0.0F);
		base_console.addChild(bone67);
		setRotationAngle(bone67, 0.0F, -0.5236F, 0.0F);
		

		bone68 = new ModelRenderer(this);
		bone68.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone67.addChild(bone68);
		setRotationAngle(bone68, -0.3927F, 0.0F, 0.0F);
		bone68.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone69 = new ModelRenderer(this);
		bone69.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone67.addChild(bone69);
		setRotationAngle(bone69, 0.0F, -1.0472F, 0.0F);
		

		bone70 = new ModelRenderer(this);
		bone70.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone69.addChild(bone70);
		setRotationAngle(bone70, -0.3927F, 0.0F, 0.0F);
		bone70.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone71 = new ModelRenderer(this);
		bone71.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone69.addChild(bone71);
		setRotationAngle(bone71, 0.0F, -1.0472F, 0.0F);
		

		bone72 = new ModelRenderer(this);
		bone72.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone71.addChild(bone72);
		setRotationAngle(bone72, -0.3927F, 0.0F, 0.0F);
		bone72.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone73 = new ModelRenderer(this);
		bone73.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone71.addChild(bone73);
		setRotationAngle(bone73, 0.0F, -1.0472F, 0.0F);
		

		bone74 = new ModelRenderer(this);
		bone74.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone73.addChild(bone74);
		setRotationAngle(bone74, -0.3927F, 0.0F, 0.0F);
		bone74.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone75 = new ModelRenderer(this);
		bone75.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone73.addChild(bone75);
		setRotationAngle(bone75, 0.0F, -1.0472F, 0.0F);
		

		bone76 = new ModelRenderer(this);
		bone76.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone75.addChild(bone76);
		setRotationAngle(bone76, -0.3927F, 0.0F, 0.0F);
		bone76.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone77 = new ModelRenderer(this);
		bone77.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone75.addChild(bone77);
		setRotationAngle(bone77, 0.0F, -1.0472F, 0.0F);
		

		bone78 = new ModelRenderer(this);
		bone78.setRotationPoint(0.0F, 18.0F, 31.73F);
		bone77.addChild(bone78);
		setRotationAngle(bone78, -0.3927F, 0.0F, 0.0F);
		bone78.setTextureOffset(42, 51).addBox(-1.0F, 0.0F, -22.0F, 2.0F, 2.0F, 22.0F, 0.0F, false);

		bone125 = new ModelRenderer(this);
		bone125.setRotationPoint(0.0F, -44.0F, 0.0F);
		base_console.addChild(bone125);
		setRotationAngle(bone125, 0.0F, -0.5236F, 0.0F);
		bone125.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone126 = new ModelRenderer(this);
		bone126.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone125.addChild(bone126);
		setRotationAngle(bone126, 0.0F, -1.0472F, 0.0F);
		bone126.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone127 = new ModelRenderer(this);
		bone127.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone126.addChild(bone127);
		setRotationAngle(bone127, 0.0F, -1.0472F, 0.0F);
		bone127.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone128 = new ModelRenderer(this);
		bone128.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone127.addChild(bone128);
		setRotationAngle(bone128, 0.0F, -1.0472F, 0.0F);
		bone128.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone129 = new ModelRenderer(this);
		bone129.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone128.addChild(bone129);
		setRotationAngle(bone129, 0.0F, -1.0472F, 0.0F);
		bone129.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone130 = new ModelRenderer(this);
		bone130.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone129.addChild(bone130);
		setRotationAngle(bone130, 0.0F, -1.0472F, 0.0F);
		bone130.setTextureOffset(80, 4).addBox(-1.0F, 9.581F, 9.4047F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		bone148 = new ModelRenderer(this);
		bone148.setRotationPoint(0.0F, -44.0F, 0.0F);
		base_console.addChild(bone148);
		bone148.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone149 = new ModelRenderer(this);
		bone149.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone148.addChild(bone149);
		setRotationAngle(bone149, 0.0F, -1.0472F, 0.0F);
		bone149.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone150 = new ModelRenderer(this);
		bone150.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone149.addChild(bone150);
		setRotationAngle(bone150, 0.0F, -1.0472F, 0.0F);
		bone150.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone151 = new ModelRenderer(this);
		bone151.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone150.addChild(bone151);
		setRotationAngle(bone151, 0.0F, -1.0472F, 0.0F);
		bone151.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone152 = new ModelRenderer(this);
		bone152.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone151.addChild(bone152);
		setRotationAngle(bone152, 0.0F, -1.0472F, 0.0F);
		bone152.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone153 = new ModelRenderer(this);
		bone153.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone152.addChild(bone153);
		setRotationAngle(bone153, 0.0F, -1.0472F, 0.0F);
		bone153.setTextureOffset(76, 97).addBox(-5.0F, 9.581F, 8.2767F, 10.0F, 10.0F, 2.0F, 0.0F, false);

		bone25 = new ModelRenderer(this);
		bone25.setRotationPoint(0.0F, -44.0F, 0.0F);
		base_console.addChild(bone25);
		

		bone26 = new ModelRenderer(this);
		bone26.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone25.addChild(bone26);
		setRotationAngle(bone26, -0.3491F, 0.0F, 0.0F);
		bone26.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone27 = new ModelRenderer(this);
		bone27.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone25.addChild(bone27);
		setRotationAngle(bone27, 0.0F, -1.0472F, 0.0F);
		

		bone28 = new ModelRenderer(this);
		bone28.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone27.addChild(bone28);
		setRotationAngle(bone28, -0.3491F, 0.0F, 0.0F);
		bone28.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone29 = new ModelRenderer(this);
		bone29.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone27.addChild(bone29);
		setRotationAngle(bone29, 0.0F, -1.0472F, 0.0F);
		

		bone30 = new ModelRenderer(this);
		bone30.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone29.addChild(bone30);
		setRotationAngle(bone30, -0.3491F, 0.0F, 0.0F);
		bone30.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone61 = new ModelRenderer(this);
		bone61.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone29.addChild(bone61);
		setRotationAngle(bone61, 0.0F, -1.0472F, 0.0F);
		

		bone62 = new ModelRenderer(this);
		bone62.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone61.addChild(bone62);
		setRotationAngle(bone62, -0.3491F, 0.0F, 0.0F);
		bone62.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone63 = new ModelRenderer(this);
		bone63.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone61.addChild(bone63);
		setRotationAngle(bone63, 0.0F, -1.0472F, 0.0F);
		

		bone64 = new ModelRenderer(this);
		bone64.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone63.addChild(bone64);
		setRotationAngle(bone64, -0.3491F, 0.0F, 0.0F);
		bone64.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone65 = new ModelRenderer(this);
		bone65.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone63.addChild(bone65);
		setRotationAngle(bone65, 0.0F, -1.0472F, 0.0F);
		

		bone66 = new ModelRenderer(this);
		bone66.setRotationPoint(-14.0F, 18.0F, 27.98F);
		bone65.addChild(bone66);
		setRotationAngle(bone66, -0.3491F, 0.0F, 0.0F);
		bone66.setTextureOffset(0, 0).addBox(-1.0F, 0.05F, -20.0F, 30.0F, 1.0F, 20.0F, 0.0F, false);

		bone55 = new ModelRenderer(this);
		bone55.setRotationPoint(0.0F, -6.0F, 0.0F);
		base_console.addChild(bone55);
		bone55.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone56 = new ModelRenderer(this);
		bone56.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone55.addChild(bone56);
		setRotationAngle(bone56, 0.0F, -1.0472F, 0.0F);
		bone56.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone57 = new ModelRenderer(this);
		bone57.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone56.addChild(bone57);
		setRotationAngle(bone57, 0.0F, -1.0472F, 0.0F);
		bone57.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone58 = new ModelRenderer(this);
		bone58.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone57.addChild(bone58);
		setRotationAngle(bone58, 0.0F, -1.0472F, 0.0F);
		bone58.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone59 = new ModelRenderer(this);
		bone59.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone58.addChild(bone59);
		setRotationAngle(bone59, 0.0F, -1.0472F, 0.0F);
		bone59.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone60 = new ModelRenderer(this);
		bone60.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone59.addChild(bone60);
		setRotationAngle(bone60, 0.0F, -1.0472F, 0.0F);
		bone60.setTextureOffset(0, 21).addBox(-15.0F, -19.0F, 9.98F, 30.0F, 1.0F, 16.0F, 0.0F, false);

		bone37 = new ModelRenderer(this);
		bone37.setRotationPoint(0.0F, -6.0F, 0.0F);
		base_console.addChild(bone37);
		setRotationAngle(bone37, 0.0F, -0.5236F, 0.0F);
		bone37.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone38 = new ModelRenderer(this);
		bone38.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone37.addChild(bone38);
		setRotationAngle(bone38, 0.0F, -1.0472F, 0.0F);
		bone38.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone39 = new ModelRenderer(this);
		bone39.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone38.addChild(bone39);
		setRotationAngle(bone39, 0.0F, -1.0472F, 0.0F);
		bone39.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone40 = new ModelRenderer(this);
		bone40.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone39.addChild(bone40);
		setRotationAngle(bone40, 0.0F, -1.0472F, 0.0F);
		bone40.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone41 = new ModelRenderer(this);
		bone41.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone40.addChild(bone41);
		setRotationAngle(bone41, 0.0F, -1.0472F, 0.0F);
		bone41.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone42 = new ModelRenderer(this);
		bone42.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone41.addChild(bone42);
		setRotationAngle(bone42, 0.0F, -1.0472F, 0.0F);
		bone42.setTextureOffset(0, 77).addBox(-1.0F, -20.0F, 29.73F, 2.0F, 3.0F, 2.0F, 0.0F, false);

		bone31 = new ModelRenderer(this);
		bone31.setRotationPoint(0.0F, -6.0F, 0.0F);
		base_console.addChild(bone31);
		bone31.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		bone32 = new ModelRenderer(this);
		bone32.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone31.addChild(bone32);
		setRotationAngle(bone32, 0.0F, -1.0472F, 0.0F);
		bone32.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		bone33 = new ModelRenderer(this);
		bone33.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone32.addChild(bone33);
		setRotationAngle(bone33, 0.0F, -1.0472F, 0.0F);
		bone33.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		bone34 = new ModelRenderer(this);
		bone34.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone33.addChild(bone34);
		setRotationAngle(bone34, 0.0F, -1.0472F, 0.0F);
		bone34.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		bone35 = new ModelRenderer(this);
		bone35.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone34.addChild(bone35);
		setRotationAngle(bone35, 0.0F, -1.0472F, 0.0F);
		bone35.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		bone36 = new ModelRenderer(this);
		bone36.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone35.addChild(bone36);
		setRotationAngle(bone36, 0.0F, -1.0472F, 0.0F);
		bone36.setTextureOffset(48, 38).addBox(-15.0F, -20.0F, 25.98F, 30.0F, 3.0F, 2.0F, 0.0F, false);

		components = new ModelRenderer(this);
		components.setRotationPoint(0.0F, 22.0F, 0.0F);
		

		north_panel = new ModelRenderer(this);
		north_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(north_panel);
		setRotationAngle(north_panel, 0.0F, 3.1416F, 0.0F);
		

		bone110 = new ModelRenderer(this);
		bone110.setRotationPoint(-14.0F, 18.0F, 27.98F);
		north_panel.addChild(bone110);
		setRotationAngle(bone110, -0.3491F, 0.0F, 0.0F);
		bone110.setTextureOffset(0, 29).addBox(5.0F, -1.95F, -11.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		bone110.setTextureOffset(94, 77).addBox(11.0F, -0.95F, -16.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone110.setTextureOffset(0, 29).addBox(19.0F, -1.95F, -11.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		bone110.setTextureOffset(68, 51).addBox(10.0F, -1.7F, -10.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		bone110.setTextureOffset(76, 33).addBox(10.0F, -0.7F, -12.0F, 8.0F, 1.0F, 1.0F, 0.0F, false);
		bone110.setTextureOffset(68, 51).addBox(15.0F, -1.7F, -10.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);
		bone110.setTextureOffset(68, 67).addBox(8.5F, -0.45F, -4.0F, 16.0F, 2.0F, 3.0F, 0.0F, false);
		bone110.setTextureOffset(80, 84).addBox(21.5F, -1.45F, -3.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		glow_button11 = new LightModelRenderer(this);
		glow_button11.setRotationPoint(10.5F, -18.5F, -18.23F);
		bone110.addChild(glow_button11);
		glow_button11.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 12.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button11.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 11.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button9 = new LightModelRenderer(this);
		glow_button9.setRotationPoint(16.5F, -18.5F, -17.23F);
		bone110.addChild(glow_button9);
		glow_button9.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button9.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button6 = new LightModelRenderer(this);
		glow_button6.setRotationPoint(25.5F, -18.5F, -17.23F);
		bone110.addChild(glow_button6);
		glow_button6.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button6.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button10 = new LightModelRenderer(this);
		glow_button10.setRotationPoint(13.5F, -18.5F, -17.23F);
		bone110.addChild(glow_button10);
		glow_button10.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button10.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button7 = new LightModelRenderer(this);
		glow_button7.setRotationPoint(22.5F, -18.5F, -17.23F);
		bone110.addChild(glow_button7);
		glow_button7.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button7.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button8 = new LightModelRenderer(this);
		glow_button8.setRotationPoint(19.5F, -18.5F, -17.23F);
		bone110.addChild(glow_button8);
		glow_button8.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button8.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		button3_rotate_X = new ModelRenderer(this);
		button3_rotate_X.setRotationPoint(10.5F, -0.45F, -66.5F);
		bone110.addChild(button3_rotate_X);
		button3_rotate_X.setTextureOffset(80, 84).addBox(-1.0F, -1.0F, 63.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		rotate1_Y27 = new ModelRenderer(this);
		rotate1_Y27.setRotationPoint(4.5F, -0.95F, -2.5F);
		bone110.addChild(rotate1_Y27);
		setRotationAngle(rotate1_Y27, 0.0F, -0.48F, 0.0F);
		rotate1_Y27.setTextureOffset(48, 51).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		button2_rotate_X = new ModelRenderer(this);
		button2_rotate_X.setRotationPoint(13.75F, -0.45F, -66.5F);
		bone110.addChild(button2_rotate_X);
		button2_rotate_X.setTextureOffset(76, 21).addBox(0.25F, -1.0F, 63.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);
		button2_rotate_X.setTextureOffset(76, 21).addBox(-1.25F, -1.0F, 63.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		doorcontrol2_rotate_x45 = new ModelRenderer(this);
		doorcontrol2_rotate_x45.setRotationPoint(17.0F, 0.05F, -2.5F);
		bone110.addChild(doorcontrol2_rotate_x45);
		setRotationAngle(doorcontrol2_rotate_x45, -0.3927F, 0.0F, 0.0F);
		doorcontrol2_rotate_x45.setTextureOffset(16, 0).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		doorcontrol1_rotate_X45 = new ModelRenderer(this);
		doorcontrol1_rotate_X45.setRotationPoint(19.0F, 0.05F, -2.5F);
		bone110.addChild(doorcontrol1_rotate_X45);
		setRotationAngle(doorcontrol1_rotate_X45, 0.3927F, 0.0F, 0.0F);
		doorcontrol1_rotate_X45.setTextureOffset(16, 0).addBox(-0.5F, -2.0F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		north_east_panel = new ModelRenderer(this);
		north_east_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(north_east_panel);
		setRotationAngle(north_east_panel, 0.0F, -2.0944F, 0.0F);
		

		bone109 = new ModelRenderer(this);
		bone109.setRotationPoint(-14.0F, 18.0F, 27.98F);
		north_east_panel.addChild(bone109);
		setRotationAngle(bone109, -0.3491F, 0.0F, 0.0F);
		bone109.setTextureOffset(0, 29).addBox(5.0F, -1.95F, -11.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		bone109.setTextureOffset(94, 77).addBox(11.0F, -0.95F, -16.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone109.setTextureOffset(0, 29).addBox(10.0F, -1.45F, -11.0F, 4.0F, 2.0F, 4.0F, 0.0F, false);
		bone109.setTextureOffset(48, 47).addBox(3.0F, -0.45F, -6.0F, 12.0F, 1.0F, 3.0F, 0.0F, false);
		bone109.setTextureOffset(92, 29).addBox(16.0F, -0.45F, -12.5F, 6.0F, 2.0F, 7.0F, 0.0F, false);
		bone109.setTextureOffset(8, 35).addBox(16.0F, -0.95F, -3.5F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button13 = new LightModelRenderer(this);
		glow_button13.setRotationPoint(3.0F, -19.5F, -18.23F);
		bone109.addChild(glow_button13);
		glow_button13.setTextureOffset(6, 0).addBox(1.0F, 18.55F, 12.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button13.setTextureOffset(75, 47).addBox(1.0F, 18.05F, 13.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button12 = new LightModelRenderer(this);
		glow_button12.setRotationPoint(11.0F, -19.5F, -18.23F);
		bone109.addChild(glow_button12);
		glow_button12.setTextureOffset(6, 0).addBox(1.0F, 18.55F, 12.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button12.setTextureOffset(75, 47).addBox(1.0F, 18.05F, 13.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button14 = new LightModelRenderer(this);
		glow_button14.setRotationPoint(7.0F, -19.5F, -28.73F);
		bone109.addChild(glow_button14);
		glow_button14.setTextureOffset(6, 0).addBox(1.0F, 18.55F, 12.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button14.setTextureOffset(75, 47).addBox(1.0F, 18.05F, 13.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		button8_rotate_X = new ModelRenderer(this);
		button8_rotate_X.setRotationPoint(19.0F, -0.2F, -79.0F);
		bone109.addChild(button8_rotate_X);
		button8_rotate_X.setTextureOffset(68, 62).addBox(-1.0F, -1.25F, 63.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);

		button4_rotate_X = new ModelRenderer(this);
		button4_rotate_X.setRotationPoint(9.0F, -0.45F, -68.5F);
		bone109.addChild(button4_rotate_X);
		button4_rotate_X.setTextureOffset(76, 24).addBox(0.5F, -1.5F, 63.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		button4_rotate_X.setTextureOffset(76, 24).addBox(-2.5F, -1.5F, 63.5F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		button6_rotate_X = new ModelRenderer(this);
		button6_rotate_X.setRotationPoint(4.5F, 0.05F, -65.5F);
		bone109.addChild(button6_rotate_X);
		button6_rotate_X.setTextureOffset(8, 6).addBox(-1.5F, -1.0F, 63.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		button5_rotate_X = new ModelRenderer(this);
		button5_rotate_X.setRotationPoint(11.25F, 0.05F, -65.5F);
		bone109.addChild(button5_rotate_X);
		button5_rotate_X.setTextureOffset(8, 6).addBox(0.75F, -1.0F, 63.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);
		button5_rotate_X.setTextureOffset(8, 6).addBox(-3.75F, -1.0F, 63.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		sonicport = new ModelRenderer(this);
		sonicport.setRotationPoint(20.5F, -19.0F, -19.73F);
		bone109.addChild(sonicport);
		sonicport.setTextureOffset(68, 51).addBox(2.0F, 18.05F, 14.23F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		rockerswitch5_rotate_X22 = new ModelRenderer(this);
		rockerswitch5_rotate_X22.setRotationPoint(20.25F, 0.05F, -3.0F);
		bone109.addChild(rockerswitch5_rotate_X22);
		setRotationAngle(rockerswitch5_rotate_X22, -0.3927F, 0.0F, 0.0F);
		rockerswitch5_rotate_X22.setTextureOffset(68, 57).addBox(0.25F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch5_rotate_X22.setTextureOffset(68, 57).addBox(-1.25F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		bone111 = new ModelRenderer(this);
		bone111.setRotationPoint(19.5F, -0.45F, -12.0F);
		bone109.addChild(bone111);
		setRotationAngle(bone111, 0.5236F, 0.0F, 0.0F);
		bone111.setTextureOffset(0, 21).addBox(-2.5F, 0.0F, 0.0F, 4.0F, 4.0F, 4.0F, 0.0F, false);

		south_east_panel = new ModelRenderer(this);
		south_east_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(south_east_panel);
		setRotationAngle(south_east_panel, 0.0F, -1.0472F, 0.0F);
		

		bone112 = new ModelRenderer(this);
		bone112.setRotationPoint(-14.0F, 18.0F, 27.98F);
		south_east_panel.addChild(bone112);
		setRotationAngle(bone112, -0.3491F, 0.0F, 0.0F);
		bone112.setTextureOffset(94, 77).addBox(11.0F, -0.95F, -16.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone112.setTextureOffset(76, 29).addBox(10.0F, -0.95F, -12.0F, 8.0F, 1.0F, 3.0F, 0.0F, false);
		bone112.setTextureOffset(68, 81).addBox(12.5F, -1.45F, -7.0F, 3.0F, 2.0F, 6.0F, 0.0F, false);
		bone112.setTextureOffset(0, 48).addBox(17.5F, -1.2F, -5.5F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		bone112.setTextureOffset(0, 38).addBox(6.5F, -1.2F, -5.5F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		bone112.setTextureOffset(68, 75).addBox(17.0F, -0.2F, -6.0F, 5.0F, 1.0F, 5.0F, 0.0F, false);
		bone112.setTextureOffset(56, 51).addBox(17.0F, -0.45F, -8.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone112.setTextureOffset(56, 51).addBox(9.0F, -0.45F, -8.5F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone112.setTextureOffset(68, 75).addBox(6.0F, -0.2F, -6.0F, 5.0F, 1.0F, 5.0F, 0.0F, true);

		switch3_rotate_X22 = new ModelRenderer(this);
		switch3_rotate_X22.setRotationPoint(15.0F, -0.45F, -10.5F);
		bone112.addChild(switch3_rotate_X22);
		switch3_rotate_X22.setTextureOffset(0, 21).addBox(1.5F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		switch3_rotate_X22.setTextureOffset(0, 21).addBox(0.0F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		switch3_rotate_X22.setTextureOffset(0, 21).addBox(-3.0F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, true);

		switch4_rotate_22 = new ModelRenderer(this);
		switch4_rotate_22.setRotationPoint(15.0F, -0.45F, -10.5F);
		bone112.addChild(switch4_rotate_22);
		setRotationAngle(switch4_rotate_22, 0.3927F, 0.0F, 0.0F);
		switch4_rotate_22.setTextureOffset(12, 21).addBox(-1.5F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		switch4_rotate_22.setTextureOffset(12, 21).addBox(-4.5F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, true);

		glow_button18 = new LightModelRenderer(this);
		glow_button18.setRotationPoint(5.5F, -20.5F, -26.48F);
		bone112.addChild(glow_button18);
		glow_button18.setTextureOffset(6, 0).addBox(1.0F, 19.55F, 13.73F, 2.0F, 1.0F, 1.0F, 0.0F, true);
		glow_button18.setTextureOffset(75, 47).addBox(1.0F, 19.05F, 14.73F, 2.0F, 2.0F, 1.0F, 0.0F, true);

		glow_button20 = new LightModelRenderer(this);
		glow_button20.setRotationPoint(1.5F, -20.5F, -18.73F);
		bone112.addChild(glow_button20);
		glow_button20.setTextureOffset(6, 0).addBox(1.0F, 19.55F, 13.73F, 2.0F, 1.0F, 1.0F, 0.0F, true);
		glow_button20.setTextureOffset(75, 47).addBox(1.0F, 19.05F, 14.73F, 2.0F, 2.0F, 1.0F, 0.0F, true);

		glow_button17 = new LightModelRenderer(this);
		glow_button17.setRotationPoint(22.5F, -20.5F, -27.48F);
		bone112.addChild(glow_button17);
		glow_button17.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button17.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button16 = new LightModelRenderer(this);
		glow_button16.setRotationPoint(24.5F, -20.5F, -23.73F);
		bone112.addChild(glow_button16);
		glow_button16.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button16.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button19 = new LightModelRenderer(this);
		glow_button19.setRotationPoint(3.5F, -20.5F, -23.73F);
		bone112.addChild(glow_button19);
		glow_button19.setTextureOffset(75, 47).addBox(1.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, true);
		glow_button19.setTextureOffset(6, 0).addBox(1.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, true);

		glow_button15 = new LightModelRenderer(this);
		glow_button15.setRotationPoint(26.5F, -20.5F, -19.73F);
		bone112.addChild(glow_button15);
		glow_button15.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button15.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		biglever_rotate_X80 = new ModelRenderer(this);
		biglever_rotate_X80.setRotationPoint(14.0F, -0.95F, -4.0F);
		bone112.addChild(biglever_rotate_X80);
		setRotationAngle(biglever_rotate_X80, 0.6981F, 0.0F, 0.0F);
		biglever_rotate_X80.setTextureOffset(30, 77).addBox(-1.0F, -4.5F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		biglever_rotate_X80.setTextureOffset(8, 8).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		directioncontrol_rotate_Y50 = new ModelRenderer(this);
		directioncontrol_rotate_Y50.setRotationPoint(10.0F, 0.05F, -7.5F);
		bone112.addChild(directioncontrol_rotate_Y50);
		setRotationAngle(directioncontrol_rotate_Y50, 0.0F, 0.4363F, 0.0F);
		directioncontrol_rotate_Y50.setTextureOffset(8, 2).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		directioncontrol_rotate_Y50.setTextureOffset(0, 35).addBox(-2.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		landtype_rotate_Y50 = new ModelRenderer(this);
		landtype_rotate_Y50.setRotationPoint(18.0F, 0.05F, -7.5F);
		bone112.addChild(landtype_rotate_Y50);
		setRotationAngle(landtype_rotate_Y50, 0.0F, -0.4363F, 0.0F);
		landtype_rotate_Y50.setTextureOffset(8, 2).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, true);
		landtype_rotate_Y50.setTextureOffset(0, 35).addBox(-0.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, true);

		south_panel = new ModelRenderer(this);
		south_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(south_panel);
		

		bone113 = new ModelRenderer(this);
		bone113.setRotationPoint(-14.0F, 18.0F, 27.98F);
		south_panel.addChild(bone113);
		setRotationAngle(bone113, -0.3491F, 0.0F, 0.0F);
		bone113.setTextureOffset(94, 77).addBox(11.0F, -0.7F, -11.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone113.setTextureOffset(68, 81).addBox(21.5F, -1.45F, -7.5F, 3.0F, 2.0F, 6.0F, 0.0F, false);
		bone113.setTextureOffset(68, 81).addBox(14.5F, -1.45F, -7.5F, 3.0F, 2.0F, 6.0F, 0.0F, false);
		bone113.setTextureOffset(0, 73).addBox(10.5F, -0.95F, -7.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		bone113.setTextureOffset(0, 73).addBox(8.25F, -0.95F, -7.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		bone113.setTextureOffset(0, 73).addBox(6.0F, -0.95F, -7.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);
		bone113.setTextureOffset(0, 73).addBox(3.75F, -0.95F, -7.5F, 2.0F, 1.0F, 3.0F, 0.0F, false);

		fastreturn_nomovement = new ModelRenderer(this);
		fastreturn_nomovement.setRotationPoint(14.0F, -19.75F, -30.23F);
		bone113.addChild(fastreturn_nomovement);
		fastreturn_nomovement.setTextureOffset(94, 77).addBox(-3.0F, 18.8F, 12.73F, 6.0F, 1.0F, 3.0F, 0.0F, false);

		glow_button23 = new LightModelRenderer(this);
		glow_button23.setRotationPoint(13.0F, -20.5F, -28.73F);
		bone113.addChild(glow_button23);
		glow_button23.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button23.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button22 = new LightModelRenderer(this);
		glow_button22.setRotationPoint(16.0F, -20.5F, -28.73F);
		bone113.addChild(glow_button22);
		glow_button22.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button22.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button21 = new LightModelRenderer(this);
		glow_button21.setRotationPoint(19.0F, -20.5F, -28.73F);
		bone113.addChild(glow_button21);
		glow_button21.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button21.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button25 = new LightModelRenderer(this);
		glow_button25.setRotationPoint(21.0F, -20.5F, -25.73F);
		bone113.addChild(glow_button25);
		glow_button25.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 13.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button25.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 14.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button24 = new LightModelRenderer(this);
		glow_button24.setRotationPoint(11.0F, -20.5F, -26.73F);
		bone113.addChild(glow_button24);
		glow_button24.setTextureOffset(75, 47).addBox(-3.0F, 19.05F, 15.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button24.setTextureOffset(6, 0).addBox(-3.0F, 19.55F, 14.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		throttle_rotate_X100 = new ModelRenderer(this);
		throttle_rotate_X100.setRotationPoint(23.0F, -0.95F, -4.5F);
		bone113.addChild(throttle_rotate_X100);
		setRotationAngle(throttle_rotate_X100, 0.8727F, 0.0F, 0.0F);
		throttle_rotate_X100.setTextureOffset(80, 0).addBox(-1.0F, -4.5F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		throttle_rotate_X100.setTextureOffset(8, 8).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		handbreak_rotate_X2 = new ModelRenderer(this);
		handbreak_rotate_X2.setRotationPoint(16.0F, -0.95F, -4.5F);
		bone113.addChild(handbreak_rotate_X2);
		setRotationAngle(handbreak_rotate_X2, 0.8727F, 0.0F, 0.0F);
		handbreak_rotate_X2.setTextureOffset(80, 0).addBox(-1.0F, -4.5F, -1.0F, 2.0F, 2.0F, 2.0F, 0.0F, false);
		handbreak_rotate_X2.setTextureOffset(8, 8).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 3.0F, 1.0F, 0.0F, false);

		button7_rotate_X = new ModelRenderer(this);
		button7_rotate_X.setRotationPoint(19.5F, -0.45F, -68.5F);
		bone113.addChild(button7_rotate_X);
		button7_rotate_X.setTextureOffset(8, 14).addBox(-1.5F, -1.0F, 62.5F, 3.0F, 2.0F, 3.0F, 0.0F, false);

		rockerswitch7_rotate_X22 = new ModelRenderer(this);
		rockerswitch7_rotate_X22.setRotationPoint(4.5F, 0.05F, -2.5F);
		bone113.addChild(rockerswitch7_rotate_X22);
		setRotationAngle(rockerswitch7_rotate_X22, 0.2182F, 0.0F, 0.0F);
		rockerswitch7_rotate_X22.setTextureOffset(68, 57).addBox(0.5F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch7_rotate_X22.setTextureOffset(68, 57).addBox(3.5F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch7_rotate_X22.setTextureOffset(68, 57).addBox(5.0F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		rockerswitch6_rotate_X22 = new ModelRenderer(this);
		rockerswitch6_rotate_X22.setRotationPoint(6.0F, 0.05F, -2.5F);
		bone113.addChild(rockerswitch6_rotate_X22);
		setRotationAngle(rockerswitch6_rotate_X22, -0.2182F, 0.0F, 0.0F);
		rockerswitch6_rotate_X22.setTextureOffset(68, 57).addBox(6.5F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch6_rotate_X22.setTextureOffset(68, 57).addBox(5.0F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch6_rotate_X22.setTextureOffset(68, 57).addBox(0.5F, -0.75F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		stabiliserspart1_rotate_X70 = new ModelRenderer(this);
		stabiliserspart1_rotate_X70.setRotationPoint(11.5F, -0.45F, -6.0F);
		bone113.addChild(stabiliserspart1_rotate_X70);
		setRotationAngle(stabiliserspart1_rotate_X70, 0.6545F, 0.0F, 0.0F);
		stabiliserspart1_rotate_X70.setTextureOffset(68, 75).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		stabiliserspart1_rotate_X70.setTextureOffset(68, 75).addBox(-2.75F, -2.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		stabiliserspart2_rotate_X70 = new ModelRenderer(this);
		stabiliserspart2_rotate_X70.setRotationPoint(11.5F, -0.45F, -6.0F);
		bone113.addChild(stabiliserspart2_rotate_X70);
		stabiliserspart2_rotate_X70.setTextureOffset(16, 6).addBox(-5.0F, -2.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);
		stabiliserspart2_rotate_X70.setTextureOffset(16, 6).addBox(-7.25F, -2.5F, -0.5F, 1.0F, 4.0F, 1.0F, 0.0F, false);

		south_west_panel = new ModelRenderer(this);
		south_west_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(south_west_panel);
		setRotationAngle(south_west_panel, 0.0F, 1.0472F, 0.0F);
		

		bone114 = new ModelRenderer(this);
		bone114.setRotationPoint(-14.0F, 18.0F, 27.98F);
		south_west_panel.addChild(bone114);
		setRotationAngle(bone114, -0.3491F, 0.0F, 0.0F);
		bone114.setTextureOffset(94, 77).addBox(11.0F, -0.95F, -16.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone114.setTextureOffset(94, 72).addBox(14.5F, -0.45F, -5.0F, 6.0F, 2.0F, 3.0F, 0.0F, false);
		bone114.setTextureOffset(56, 51).addBox(13.0F, -0.45F, -12.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone114.setTextureOffset(56, 51).addBox(22.0F, -0.45F, -6.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone114.setTextureOffset(56, 51).addBox(24.0F, -0.45F, -3.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone114.setTextureOffset(0, 48).addBox(7.5F, -0.2F, -5.5F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		bone114.setTextureOffset(8, 0).addBox(19.5F, -0.95F, -13.0F, 2.0F, 2.0F, 4.0F, 0.0F, false);

		button1_rotate_X = new ModelRenderer(this);
		button1_rotate_X.setRotationPoint(16.125F, -0.825F, -67.5F);
		bone114.addChild(button1_rotate_X);
		button1_rotate_X.setTextureOffset(12, 29).addBox(0.125F, -0.375F, 64.25F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		button1_rotate_X.setTextureOffset(12, 29).addBox(-1.125F, -0.375F, 64.25F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		button1_rotate_X.setTextureOffset(12, 31).addBox(-1.125F, -0.625F, 62.75F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		button1_rotate_X.setTextureOffset(12, 31).addBox(0.125F, -0.625F, 62.75F, 1.0F, 1.0F, 1.0F, 0.0F, false);

		rotate_Y27 = new ModelRenderer(this);
		rotate_Y27.setRotationPoint(19.0F, -0.95F, -3.5F);
		bone114.addChild(rotate_Y27);
		setRotationAngle(rotate_Y27, 0.0F, -0.48F, 0.0F);
		rotate_Y27.setTextureOffset(80, 81).addBox(-1.0F, -0.5F, -1.0F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		x_increment = new LightModelRenderer(this);
		x_increment.setRotationPoint(21.0F, -19.5F, -24.23F);
		bone114.addChild(x_increment);
		x_increment.setTextureOffset(6, 0).addBox(-5.0F, 18.55F, 12.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		x_increment.setTextureOffset(75, 47).addBox(-5.0F, 18.05F, 13.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		Y_increment = new LightModelRenderer(this);
		Y_increment.setRotationPoint(18.0F, -19.5F, -21.23F);
		bone114.addChild(Y_increment);
		Y_increment.setTextureOffset(6, 0).addBox(-5.0F, 18.55F, 12.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		Y_increment.setTextureOffset(75, 47).addBox(-5.0F, 18.05F, 13.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		Z_increment = new LightModelRenderer(this);
		Z_increment.setRotationPoint(15.0F, -19.0F, -25.23F);
		bone114.addChild(Z_increment);
		Z_increment.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 14.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		Z_increment.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 13.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		spin7_rotate_Y55 = new ModelRenderer(this);
		spin7_rotate_Y55.setRotationPoint(14.0F, 0.05F, -11.0F);
		bone114.addChild(spin7_rotate_Y55);
		setRotationAngle(spin7_rotate_Y55, 0.0F, -1.9199F, 0.0F);
		spin7_rotate_Y55.setTextureOffset(8, 2).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin7_rotate_Y55.setTextureOffset(0, 35).addBox(-2.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		spin6_rotate_aY55 = new ModelRenderer(this);
		spin6_rotate_aY55.setRotationPoint(23.0F, 0.05F, -5.0F);
		bone114.addChild(spin6_rotate_aY55);
		setRotationAngle(spin6_rotate_aY55, 0.0F, 0.9163F, 0.0F);
		spin6_rotate_aY55.setTextureOffset(8, 2).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin6_rotate_aY55.setTextureOffset(0, 35).addBox(-2.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		spin5_rotate_Y55 = new ModelRenderer(this);
		spin5_rotate_Y55.setRotationPoint(25.0F, 0.05F, -2.0F);
		bone114.addChild(spin5_rotate_Y55);
		setRotationAngle(spin5_rotate_Y55, 0.0F, 0.9163F, 0.0F);
		spin5_rotate_Y55.setTextureOffset(8, 2).addBox(-0.5F, -1.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin5_rotate_Y55.setTextureOffset(0, 35).addBox(-2.5F, -2.0F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		button_rotate_X = new ModelRenderer(this);
		button_rotate_X.setRotationPoint(9.5F, -0.2F, -67.5F);
		bone114.addChild(button_rotate_X);
		button_rotate_X.setTextureOffset(30, 73).addBox(-1.5F, -1.0F, 62.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		glow_button = new LightModelRenderer(this);
		glow_button.setRotationPoint(16.5F, -20.0F, -23.73F);
		bone114.addChild(glow_button);
		glow_button.setTextureOffset(75, 47).addBox(-9.0F, 18.55F, 16.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button.setTextureOffset(6, 0).addBox(-9.0F, 19.05F, 15.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		rockerswitch4_rotate_X22 = new ModelRenderer(this);
		rockerswitch4_rotate_X22.setRotationPoint(8.0F, 0.05F, -10.5F);
		bone114.addChild(rockerswitch4_rotate_X22);
		setRotationAngle(rockerswitch4_rotate_X22, -0.2182F, 0.0F, 0.0F);
		rockerswitch4_rotate_X22.setTextureOffset(68, 57).addBox(-0.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		rockerswitch2_rotate_X22 = new ModelRenderer(this);
		rockerswitch2_rotate_X22.setRotationPoint(4.5F, 0.05F, -3.5F);
		bone114.addChild(rockerswitch2_rotate_X22);
		setRotationAngle(rockerswitch2_rotate_X22, -0.2182F, 0.0F, 0.0F);
		rockerswitch2_rotate_X22.setTextureOffset(68, 57).addBox(-0.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch2_rotate_X22.setTextureOffset(68, 57).addBox(1.0F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		rockerswitch3_rotate_X22 = new ModelRenderer(this);
		rockerswitch3_rotate_X22.setRotationPoint(3.0F, 0.05F, -3.5F);
		bone114.addChild(rockerswitch3_rotate_X22);
		setRotationAngle(rockerswitch3_rotate_X22, 0.3927F, 0.0F, 0.0F);
		rockerswitch3_rotate_X22.setTextureOffset(68, 57).addBox(-0.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		increment_rotate_X60 = new ModelRenderer(this);
		increment_rotate_X60.setRotationPoint(20.5F, -0.45F, -11.0F);
		bone114.addChild(increment_rotate_X60);
		setRotationAngle(increment_rotate_X60, 0.5236F, 0.0F, 0.0F);
		increment_rotate_X60.setTextureOffset(16, 0).addBox(-0.5F, -2.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		north_west_panel = new ModelRenderer(this);
		north_west_panel.setRotationPoint(0.0F, -44.0F, 0.0F);
		components.addChild(north_west_panel);
		setRotationAngle(north_west_panel, 0.0F, 2.0944F, 0.0F);
		

		bone118 = new ModelRenderer(this);
		bone118.setRotationPoint(-14.0F, 18.0F, 27.98F);
		north_west_panel.addChild(bone118);
		setRotationAngle(bone118, -0.3491F, 0.0F, 0.0F);
		bone118.setTextureOffset(94, 77).addBox(11.0F, -0.95F, -16.5F, 6.0F, 1.0F, 3.0F, 0.0F, false);
		bone118.setTextureOffset(0, 43).addBox(5.5F, -0.7F, -12.0F, 4.0F, 1.0F, 4.0F, 0.0F, false);
		bone118.setTextureOffset(12, 89).addBox(17.0F, -0.95F, -12.5F, 2.0F, 2.0F, 10.0F, 0.0F, false);
		bone118.setTextureOffset(30, 73).addBox(10.0F, -0.45F, -3.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		bone118.setTextureOffset(8, 8).addBox(15.0F, -0.95F, -6.5F, 2.0F, 2.0F, 4.0F, 0.0F, false);
		bone118.setTextureOffset(56, 51).addBox(23.6987F, -0.5673F, -3.2367F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone118.setTextureOffset(56, 51).addBox(22.1987F, -0.5673F, -6.4867F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone118.setTextureOffset(56, 51).addBox(13.4487F, -0.5673F, -11.2367F, 2.0F, 1.0F, 2.0F, 0.0F, false);
		bone118.setTextureOffset(56, 51).addBox(10.1987F, -0.5673F, -11.2367F, 2.0F, 1.0F, 2.0F, 0.0F, false);

		randomiser_rotate_X = new ModelRenderer(this);
		randomiser_rotate_X.setRotationPoint(7.5F, -0.95F, -74.0F);
		bone118.addChild(randomiser_rotate_X);
		randomiser_rotate_X.setTextureOffset(8, 89).addBox(-1.5F, -0.5F, 62.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);

		glow_button3 = new LightModelRenderer(this);
		glow_button3.setRotationPoint(24.5F, -18.5F, -22.23F);
		bone118.addChild(glow_button3);
		glow_button3.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button3.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button4 = new LightModelRenderer(this);
		glow_button4.setRotationPoint(24.5F, -18.5F, -20.23F);
		bone118.addChild(glow_button4);
		glow_button4.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 12.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);
		glow_button4.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 11.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);

		glow_button5 = new LightModelRenderer(this);
		glow_button5.setRotationPoint(24.5F, -18.5F, -16.23F);
		bone118.addChild(glow_button5);
		glow_button5.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button5.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		glow_button2 = new LightModelRenderer(this);
		glow_button2.setRotationPoint(15.5F, -18.5F, -17.23F);
		bone118.addChild(glow_button2);
		glow_button2.setTextureOffset(6, 0).addBox(-5.0F, 18.05F, 10.73F, 2.0F, 1.0F, 1.0F, 0.0F, false);
		glow_button2.setTextureOffset(75, 47).addBox(-5.0F, 17.55F, 11.73F, 2.0F, 2.0F, 1.0F, 0.0F, false);

		switch_rotate_35 = new ModelRenderer(this);
		switch_rotate_35.setRotationPoint(11.5F, -0.45F, -2.0F);
		bone118.addChild(switch_rotate_35);
		setRotationAngle(switch_rotate_35, 0.6109F, 0.0F, 0.0F);
		switch_rotate_35.setTextureOffset(16, 0).addBox(-0.5F, -1.5F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);

		rockerswitch1_rotate_22 = new ModelRenderer(this);
		rockerswitch1_rotate_22.setRotationPoint(6.0F, 0.05F, -3.05F);
		bone118.addChild(rockerswitch1_rotate_22);
		setRotationAngle(rockerswitch1_rotate_22, 0.2182F, 0.0F, 0.0F);
		rockerswitch1_rotate_22.setTextureOffset(68, 57).addBox(0.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);
		rockerswitch1_rotate_22.setTextureOffset(68, 57).addBox(-1.5F, -1.0F, -1.5F, 1.0F, 2.0F, 3.0F, 0.0F, false);

		refuel1_rotate_45 = new ModelRenderer(this);
		refuel1_rotate_45.setRotationPoint(18.0F, -0.7F, -10.5F);
		bone118.addChild(refuel1_rotate_45);
		setRotationAngle(refuel1_rotate_45, 0.3927F, 0.0F, 0.0F);
		refuel1_rotate_45.setTextureOffset(0, 29).addBox(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		refuel1_rotate_45.setTextureOffset(76, 21).addBox(-0.5F, -2.75F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		refuel2_rotate_45 = new ModelRenderer(this);
		refuel2_rotate_45.setRotationPoint(18.0F, -0.7F, -7.5F);
		bone118.addChild(refuel2_rotate_45);
		setRotationAngle(refuel2_rotate_45, 0.3927F, 0.0F, 0.0F);
		refuel2_rotate_45.setTextureOffset(0, 29).addBox(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		refuel2_rotate_45.setTextureOffset(76, 21).addBox(-0.5F, -2.75F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		refuel3_rotate_45 = new ModelRenderer(this);
		refuel3_rotate_45.setRotationPoint(18.0F, -0.7F, -4.5F);
		bone118.addChild(refuel3_rotate_45);
		setRotationAngle(refuel3_rotate_45, 0.3927F, 0.0F, 0.0F);
		refuel3_rotate_45.setTextureOffset(0, 29).addBox(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		refuel3_rotate_45.setTextureOffset(76, 21).addBox(-0.5F, -2.75F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		lever4_rotate_45 = new ModelRenderer(this);
		lever4_rotate_45.setRotationPoint(16.0F, -0.7F, -4.5F);
		bone118.addChild(lever4_rotate_45);
		setRotationAngle(lever4_rotate_45, -0.3927F, 0.0F, 0.0F);
		lever4_rotate_45.setTextureOffset(0, 29).addBox(-0.5F, -1.75F, -0.5F, 1.0F, 2.0F, 1.0F, 0.0F, false);
		lever4_rotate_45.setTextureOffset(76, 21).addBox(-0.5F, -2.75F, -1.0F, 1.0F, 1.0F, 2.0F, 0.0F, false);

		spin3_rotate_55 = new ModelRenderer(this);
		spin3_rotate_55.setRotationPoint(24.6987F, -0.3173F, -2.2367F);
		bone118.addChild(spin3_rotate_55);
		setRotationAngle(spin3_rotate_55, 0.0F, 0.6109F, 0.0F);
		spin3_rotate_55.setTextureOffset(8, 2).addBox(-0.5F, -0.75F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin3_rotate_55.setTextureOffset(0, 35).addBox(-2.5F, -1.75F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		spin4_rotate_55 = new ModelRenderer(this);
		spin4_rotate_55.setRotationPoint(23.1987F, -0.3173F, -5.4867F);
		bone118.addChild(spin4_rotate_55);
		setRotationAngle(spin4_rotate_55, 0.0F, 0.6109F, 0.0F);
		spin4_rotate_55.setTextureOffset(8, 2).addBox(-0.5F, -0.75F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin4_rotate_55.setTextureOffset(0, 35).addBox(-2.5F, -1.75F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		spin_rotate_55 = new ModelRenderer(this);
		spin_rotate_55.setRotationPoint(14.4487F, -0.3173F, -10.2367F);
		bone118.addChild(spin_rotate_55);
		setRotationAngle(spin_rotate_55, 0.0F, 0.9599F, 0.0F);
		spin_rotate_55.setTextureOffset(8, 2).addBox(-0.5F, -0.75F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin_rotate_55.setTextureOffset(0, 35).addBox(-2.5F, -1.75F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		spin2_rotate_55 = new ModelRenderer(this);
		spin2_rotate_55.setRotationPoint(11.1987F, -0.3173F, -10.2367F);
		bone118.addChild(spin2_rotate_55);
		setRotationAngle(spin2_rotate_55, 0.0F, 0.9599F, 0.0F);
		spin2_rotate_55.setTextureOffset(8, 2).addBox(-0.5F, -0.75F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		spin2_rotate_55.setTextureOffset(0, 35).addBox(-2.5F, -1.75F, -0.5F, 3.0F, 1.0F, 1.0F, 0.0F, false);

		rotor_translate_8 = new ModelRenderer(this);
		rotor_translate_8.setRotationPoint(0.0F, 22.0F, 0.0F);
		

		bone94 = new ModelRenderer(this);
		bone94.setRotationPoint(0.0F, -43.0976F, 0.0F);
		rotor_translate_8.addChild(bone94);
		setRotationAngle(bone94, 0.0F, -0.5236F, 0.0F);
		bone94.setTextureOffset(0, 38).addBox(-8.0F, -8.2F, -8.0F, 16.0F, 19.0F, 16.0F, 0.0F, false);

		inner_rotor_rotate_y = new ModelRenderer(this);
		inner_rotor_rotate_y.setRotationPoint(0.0F, -38.0F, 0.0F);
		rotor_translate_8.addChild(inner_rotor_rotate_y);
		inner_rotor_rotate_y.setTextureOffset(0, 73).addBox(-5.0F, -2.2976F, -5.0F, 10.0F, 6.0F, 10.0F, 0.0F, false);
		inner_rotor_rotate_y.setTextureOffset(0, 0).addBox(-1.0F, -13.2976F, -1.0F, 2.0F, 18.0F, 2.0F, 0.0F, false);
		inner_rotor_rotate_y.setTextureOffset(80, 0).addBox(-1.0F, -16.2976F, -5.0F, 2.0F, 4.0F, 10.0F, 0.0F, false);

		bone95 = new ModelRenderer(this);
		bone95.setRotationPoint(0.0F, -6.2976F, 0.0F);
		inner_rotor_rotate_y.addChild(bone95);
		setRotationAngle(bone95, 0.0F, -0.5236F, 0.0F);
		bone95.setTextureOffset(94, 0).addBox(-5.0F, -4.0F, -1.0F, 10.0F, 8.0F, 2.0F, 0.0F, false);

		bone134 = new ModelRenderer(this);
		bone134.setRotationPoint(0.0F, -44.0F, 0.0F);
		rotor_translate_8.addChild(bone134);
		bone134.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);

		bone135 = new ModelRenderer(this);
		bone135.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone134.addChild(bone135);
		setRotationAngle(bone135, 0.0F, -1.0472F, 0.0F);
		bone135.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);

		bone136 = new ModelRenderer(this);
		bone136.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone135.addChild(bone136);
		setRotationAngle(bone136, 0.0F, -1.0472F, 0.0F);
		bone136.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);

		bone143 = new ModelRenderer(this);
		bone143.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone136.addChild(bone143);
		setRotationAngle(bone143, 0.0F, -1.0472F, 0.0F);
		bone143.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);

		bone144 = new ModelRenderer(this);
		bone144.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone143.addChild(bone144);
		setRotationAngle(bone144, 0.0F, -1.0472F, 0.0F);
		bone144.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);

		bone145 = new ModelRenderer(this);
		bone145.setRotationPoint(0.0F, 0.0F, 0.0F);
		bone144.addChild(bone145);
		setRotationAngle(bone145, 0.0F, -1.0472F, 0.0F);
		bone145.setTextureOffset(68, 57).addBox(-5.0F, 11.581F, -0.7233F, 10.0F, 1.0F, 9.0F, 0.0F, false);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(HartnellConsoleTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha) {
		matrixStack.push();
		matrixStack.scale(scale, scale, scale);
        
		this.inner_rotor_rotate_y.rotateAngleY = (float) Math.toRadians(tile.flightTicks * 2 % 360);
		
		matrixStack.push();
		matrixStack.translate(0, (float) Math.cos(tile.flightTicks * 0.1) * 0.25F + 0.25F, 0);
		this.rotor_translate_8.render(matrixStack, buffer, packedLight, packedOverlay);
        matrixStack.pop();
        
        tile.getControl(ThrottleControl.class).ifPresent(throttle -> {
        	this.throttle_rotate_X100.rotateAngleX = (float) Math.toRadians(-50 + (throttle.getAmount() * 90.0F));
        });

        tile.getControl(HandbrakeControl.class).ifPresent(handBrake -> {
        	this.handbreak_rotate_X2.rotateAngleX = (float) Math.toRadians(handBrake.isFree() ? -45F : 40F);
        });
        
        tile.getControl(XControl.class).ifPresent(x -> {
			this.x_increment.setBright(x.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(YControl.class).ifPresent(y -> {
			this.Y_increment.setBright(y.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});
		
		tile.getControl(ZControl.class).ifPresent(z -> {
			this.Z_increment.setBright(z.getAnimationTicks() != 0 ? 1.0F : 0.0F);
		});

        tile.getControl(IncModControl.class).ifPresent(inc -> {
        	this.increment_rotate_X60.rotateAngleX = -0.75F * (inc.index / (float) IncModControl.COORD_MODS.length);	
        });
        

        tile.getControl(LandingTypeControl.class).ifPresent(land -> {
        	this.landtype_rotate_Y50.rotateAngleY = -50F * (land.getLandType() == EnumLandType.UP ? 50 : 0);
        });
        
        tile.getSubsystem(StabilizerSubsystem.class).ifPresent(sys -> {
			this.stabiliserspart1_rotate_X70.rotateAngleX = (float)Math.toRadians(sys.isControlActivated() ? 50 : -50);
			this.stabiliserspart2_rotate_X70.rotateAngleX = (float)Math.toRadians(sys.isControlActivated() ? -50 : 50);
		});
        
        tile.getControl(FacingControl.class).ifPresent(facing -> {
        	float angle = WorldHelper.getAngleFromFacing(tile.getExteriorFacingDirection());
        	this.directioncontrol_rotate_Y50.rotateAngleY = (float) Math.toRadians(angle += facing.getAnimationProgress() * 360.0F);
        });
        

        tile.getControl(RandomiserControl.class).ifPresent(rand -> {
            this.randomiser_rotate_X.rotateAngleX = (float) -Math.toRadians(rand.getAnimationTicks() != 0 ? 0.5 : 0);	
        });
        
        tile.getControl(DoorControl.class).ifPresent(doorControl -> {
            float angle = 0;
			
			DoorEntity door = tile.getDoor().orElse(null);
			if(door != null)
				angle = door.getOpenState() == EnumDoorState.CLOSED ? -45 : 45;
			
			this.doorcontrol1_rotate_X45.rotateAngleX = (float)Math.toRadians(-angle);
			this.doorcontrol2_rotate_x45.rotateAngleX = (float)Math.toRadians(angle);
        });
        
        int glowTime = (int)(tile.getWorld().getGameTime() % 120);
        
        this.glow_button.setBright(1F);
        
        this.glow_button25.setBright(1F);
        this.glow_button21.setBright(1F);
        this.glow_button22.setBright(1F);
        this.glow_button23.setBright(1F);
        this.glow_button24.setBright(tile.getWorld().getGameTime() % 180 > 60 ? 1.0F : 0.0F);
        
        this.glow_button2.setBright(glowTime < 70 ? 1.0F : 0.0F);
        this.glow_button3.setBright(glowTime > 30 ? 1.0F : 0.0F);
        this.glow_button4.setBright(glowTime < 30 ? 1.0F : 0.0F);
        this.glow_button5.setBright(glowTime > 30 ? 1.0F : 0.0F);
        
        this.glow_button6.setBright(glowTime < 30 ? 1.0F : 0.0F);
        this.glow_button7.setBright(glowTime > 30 ? 1.0F : 0.0F);
        this.glow_button8.setBright(glowTime < 30 ? 1.0F : 0.0F);
        this.glow_button9.setBright(glowTime > 30 ? 1.0F : 0.0F);
        this.glow_button10.setBright(glowTime < 30 ? 1.0F : 0.0F);
        this.glow_button11.setBright(glowTime > 30 ? 1.0F : 0.0F);
        
        int glowTime2 = (int)(tile.getWorld().getGameTime() % 100);
        this.glow_button15.setBright(1F);
        this.glow_button16.setBright(glowTime2 > 60 ? 1.0F : 0.0F);
        this.glow_button17.setBright(1F);
        this.glow_button18.setBright(1F);
        this.glow_button19.setBright(1F);
        this.glow_button20.setBright(glowTime2 < 70 ? 1.0F : 0.0F);
        
        this.glow_button12.setBright(glowTime2 < 70 ? 1.0F : 0.0F);
        this.glow_button13.setBright(glowTime2 > 70 ? 1.0F : 0.0F);
        this.glow_button14.setBright(1F);
        
        this.spin2_rotate_55.rotateAngleY = MathHelper.clamp(MathHelper.sin((Minecraft.getInstance().player.ticksExisted + Minecraft.getInstance().getRenderPartialTicks()) / 60) * 10, 0, 9);
        
        this.spin5_rotate_Y55.rotateAngleY = MathHelper.clamp(-MathHelper.sin((Minecraft.getInstance().player.ticksExisted + Minecraft.getInstance().getRenderPartialTicks()) / 60) * 10, 0, 9);
        
        base_console.render(matrixStack, buffer, packedLight, packedOverlay);
        components.render(matrixStack, buffer, packedLight, packedOverlay);
		matrixStack.pop();
	}
}