package net.tardis.mod.client.models.exteriors;
// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.renderers.boti.BOTIRenderer;
import net.tardis.mod.client.renderers.boti.PortalInfo;
import net.tardis.mod.client.renderers.exteriors.ExteriorRenderer;
import net.tardis.mod.client.renderers.exteriors.ModernPoliceBoxExteriorRenderer;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.enums.EnumMatterState;
import net.tardis.mod.helper.WorldHelper;
import net.tardis.mod.misc.IDoorType;
//import net.tardis.mod.client.models.IExteriorModel;
//import net.tardis.mod.entity.TardisEntity;
//import net.tardis.mod.enums.EnumDoorState;
//import net.tardis.mod.enums.EnumMatterState;
//import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

public class ModernPoliceBoxExteriorModel extends ExteriorModel{
	private static float LIGHT = 0F;
    private static float ALPHA = 1F;
	private final ModelRenderer base;
	private final ModelRenderer doors;
	private final ModelRenderer left2;
	private final ModelRenderer window3;
	private final ModelRenderer frame3;
	private final ModelRenderer glow2;
	private final ModelRenderer right2;
	private final ModelRenderer window4;
	private final ModelRenderer frame4;
	private final ModelRenderer glow5;
	private final ModelRenderer side;
	private final ModelRenderer left;
	private final ModelRenderer window;
	private final ModelRenderer frame;
	private final ModelRenderer glow3;
	private final ModelRenderer right;
	private final ModelRenderer window2;
	private final ModelRenderer frame2;
	private final ModelRenderer glow4;
	private final ModelRenderer side2;
	private final ModelRenderer left3;
	private final ModelRenderer window5;
	private final ModelRenderer frame5;
	private final ModelRenderer glow6;
	private final ModelRenderer right3;
	private final ModelRenderer window6;
	private final ModelRenderer frame6;
	private final ModelRenderer glow7;
	private final ModelRenderer side3;
	private final ModelRenderer left4;
	private final ModelRenderer window7;
	private final ModelRenderer frame7;
	private final ModelRenderer glow8;
	private final ModelRenderer right4;
	private final ModelRenderer window8;
	private final ModelRenderer frame8;
	private final ModelRenderer glow10;
	private final ModelRenderer roof;
	private final ModelRenderer lamp;
	private final ModelRenderer glow9;
	private final ModelRenderer boti;
	private final ModelRenderer backing;

	public ModernPoliceBoxExteriorModel() {
		textureWidth = 151;
		textureHeight = 151;

		base = new ModelRenderer(this);
		base.setRotationPoint(0.0F, 24.0F, 0.0F);
		base.setTextureOffset(0, 0).addBox(-11.0F, -2.0F, -11.0F, 22.0F, 2.0F, 22.0F, 0.0F, false);

		doors = new ModelRenderer(this);
		doors.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(doors, 0.0F, 1.5708F, 0.0F);
		doors.setTextureOffset(76, 64).addBox(8.5F, -32.5F, -8.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
		doors.setTextureOffset(66, 0).addBox(7.75F, -33.0F, -8.0F, 2.0F, 1.0F, 16.0F, 0.0F, false);
		doors.setTextureOffset(0, 66).addBox(8.75F, -35.5F, -9.0F, 2.0F, 3.0F, 18.0F, 0.0F, false);
		doors.setTextureOffset(66, 85).addBox(8.0F, -36.0F, 8.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
		doors.setTextureOffset(22, 54).addBox(11.0F, -35.0F, -7.0F, 0.0F, 2.0F, 14.0F, 0.0F, false);

		left2 = new ModelRenderer(this);
		left2.setRotationPoint(8.0F, -1.0F, 8.0F);
		doors.addChild(left2);
		left2.setTextureOffset(92, 6).addBox(0.0F, -2.0F, -6.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(86, 86).addBox(0.0F, -31.0F, -1.0F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left2.setTextureOffset(16, 87).addBox(0.0F, -31.0F, -7.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left2.setTextureOffset(91, 25).addBox(0.0F, -31.0F, -6.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(99, 21).addBox(0.0F, -9.0F, -6.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(99, 21).addBox(0.0F, -16.0F, -6.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(98, 92).addBox(0.0F, -23.0F, -6.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(36, 88).addBox(0.75F, -8.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(24, 88).addBox(0.25F, -8.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(36, 88).addBox(0.75F, -15.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(24, 88).addBox(0.25F, -15.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(36, 88).addBox(0.75F, -22.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(24, 88).addBox(0.25F, -22.0F, -6.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left2.setTextureOffset(64, 59).addBox(0.5F, -17.0F, -7.0F, 1.0F, 3.0F, 0.0F, 0.0F, false);

		window3 = new ModelRenderer(this);
		window3.setRotationPoint(-8.25F, 1.0F, -7.75F);
		left2.addChild(window3);
		

		frame3 = new ModelRenderer(this);
		frame3.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(frame3);
		frame3.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		frame3.setTextureOffset(0, 66).addBox(8.5F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow2 = new ModelRenderer(this);
		glow2.setRotationPoint(0.0F, 0.0F, 0.0F);
		window3.addChild(glow2);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow2.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		right2 = new ModelRenderer(this);
		right2.setRotationPoint(8.0F, -1.0F, -8.0F);
		doors.addChild(right2);
		right2.setTextureOffset(92, 6).addBox(0.0F, -2.0F, 0.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(20, 87).addBox(0.0F, -31.0F, 6.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right2.setTextureOffset(16, 87).addBox(0.0F, -31.0F, 0.0F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right2.setTextureOffset(91, 25).addBox(0.0F, -31.0F, 0.75F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(99, 21).addBox(0.0F, -9.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(99, 21).addBox(0.0F, -16.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(98, 92).addBox(0.0F, -23.0F, 0.75F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(36, 82).addBox(0.75F, -8.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(24, 82).addBox(0.25F, -8.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(36, 82).addBox(0.75F, -15.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(24, 82).addBox(0.25F, -15.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(24, 82).addBox(0.25F, -22.0F, 0.75F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right2.setTextureOffset(0, 0).addBox(0.75F, -22.5F, 0.25F, 0.0F, 7.0F, 7.0F, 0.0F, false);
		right2.setTextureOffset(0, 0).addBox(1.0F, -21.0F, 2.25F, 0.0F, 3.0F, 3.0F, 0.0F, false);
		right2.setTextureOffset(14, 14).addBox(1.0F, -20.0F, 2.75F, 0.0F, 3.0F, 2.0F, 0.0F, false);
		right2.setTextureOffset(68, 14).addBox(0.25F, -20.0F, 1.25F, 1.0F, 2.0F, 0.0F, 0.0F, false);
		right2.setTextureOffset(12, 87).addBox(0.25F, -31.0F, 7.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);

		window4 = new ModelRenderer(this);
		window4.setRotationPoint(-8.25F, 1.0F, -0.25F);
		right2.addChild(window4);
		

		frame4 = new ModelRenderer(this);
		frame4.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(frame4);
		frame4.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		frame4.setTextureOffset(0, 66).addBox(8.5F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow5 = new ModelRenderer(this);
		glow5.setRotationPoint(0.0F, 0.0F, 0.0F);
		window4.addChild(glow5);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 37).addBox(8.375F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow5.setTextureOffset(63, 38).addBox(8.375F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		side = new ModelRenderer(this);
		side.setRotationPoint(0.0F, 23.0F, 0.0F);
		side.setTextureOffset(66, 85).addBox(8.0F, -36.0F, 8.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
		side.setTextureOffset(12, 87).addBox(8.25F, -32.0F, -0.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		side.setTextureOffset(76, 64).addBox(8.5F, -32.5F, -8.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
		side.setTextureOffset(66, 0).addBox(7.75F, -33.0F, -8.0F, 2.0F, 1.0F, 16.0F, 0.0F, false);
		side.setTextureOffset(0, 66).addBox(8.75F, -35.5F, -9.0F, 2.0F, 3.0F, 18.0F, 0.0F, false);
		side.setTextureOffset(22, 52).addBox(11.0F, -35.0F, -7.0F, 0.0F, 2.0F, 14.0F, 0.0F, false);

		left = new ModelRenderer(this);
		left.setRotationPoint(0.0F, 0.0F, 0.25F);
		side.addChild(left);
		left.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(36, 88).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(36, 88).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left.setTextureOffset(36, 88).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window = new ModelRenderer(this);
		window.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left.addChild(window);
		

		frame = new ModelRenderer(this);
		frame.setRotationPoint(0.0F, 0.0F, 0.0F);
		window.addChild(frame);
		frame.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow3 = new ModelRenderer(this);
		glow3.setRotationPoint(0.0F, 0.0F, 0.0F);
		window.addChild(glow3);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow3.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		right = new ModelRenderer(this);
		right.setRotationPoint(0.0F, 0.0F, -8.25F);
		side.addChild(right);
		right.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(36, 82).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(36, 82).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right.setTextureOffset(36, 82).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window2 = new ModelRenderer(this);
		window2.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right.addChild(window2);
		

		frame2 = new ModelRenderer(this);
		frame2.setRotationPoint(0.0F, 0.0F, 0.0F);
		window2.addChild(frame2);
		frame2.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow4 = new ModelRenderer(this);
		glow4.setRotationPoint(0.0F, 0.0F, 0.0F);
		window2.addChild(glow4);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow4.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		side2 = new ModelRenderer(this);
		side2.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(side2, 0.0F, -1.5708F, 0.0F);
		side2.setTextureOffset(66, 85).addBox(8.0F, -36.0F, 8.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
		side2.setTextureOffset(12, 87).addBox(8.25F, -32.0F, -0.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		side2.setTextureOffset(76, 64).addBox(8.5F, -32.5F, -8.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
		side2.setTextureOffset(66, 0).addBox(7.75F, -33.0F, -8.0F, 2.0F, 1.0F, 16.0F, 0.0F, false);
		side2.setTextureOffset(0, 66).addBox(8.75F, -35.5F, -9.0F, 2.0F, 3.0F, 18.0F, 0.0F, false);
		side2.setTextureOffset(22, 52).addBox(11.0F, -35.0F, -7.0F, 0.0F, 2.0F, 14.0F, 0.0F, false);

		left3 = new ModelRenderer(this);
		left3.setRotationPoint(0.0F, 0.0F, 0.25F);
		side2.addChild(left3);
		left3.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left3.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left3.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(36, 88).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(36, 88).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left3.setTextureOffset(36, 88).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window5 = new ModelRenderer(this);
		window5.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left3.addChild(window5);
		

		frame5 = new ModelRenderer(this);
		frame5.setRotationPoint(0.0F, 0.0F, 0.0F);
		window5.addChild(frame5);
		frame5.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow6 = new ModelRenderer(this);
		glow6.setRotationPoint(0.0F, 0.0F, 0.0F);
		window5.addChild(glow6);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow6.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		right3 = new ModelRenderer(this);
		right3.setRotationPoint(0.0F, 0.0F, -8.25F);
		side2.addChild(right3);
		right3.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right3.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right3.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(36, 82).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(36, 82).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right3.setTextureOffset(36, 82).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window6 = new ModelRenderer(this);
		window6.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right3.addChild(window6);
		

		frame6 = new ModelRenderer(this);
		frame6.setRotationPoint(0.0F, 0.0F, 0.0F);
		window6.addChild(frame6);
		frame6.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow7 = new ModelRenderer(this);
		glow7.setRotationPoint(0.0F, 0.0F, 0.0F);
		window6.addChild(glow7);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow7.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		side3 = new ModelRenderer(this);
		side3.setRotationPoint(0.0F, 23.0F, 0.0F);
		setRotationAngle(side3, 0.0F, 3.1416F, 0.0F);
		side3.setTextureOffset(66, 85).addBox(8.0F, -36.0F, 8.0F, 2.0F, 35.0F, 2.0F, 0.0F, false);
		side3.setTextureOffset(12, 87).addBox(8.25F, -32.0F, -0.5F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		side3.setTextureOffset(76, 64).addBox(8.5F, -32.5F, -8.0F, 1.0F, 1.0F, 16.0F, 0.0F, false);
		side3.setTextureOffset(66, 0).addBox(7.75F, -33.0F, -8.0F, 2.0F, 1.0F, 16.0F, 0.0F, false);
		side3.setTextureOffset(0, 66).addBox(8.75F, -35.5F, -9.0F, 2.0F, 3.0F, 18.0F, 0.0F, false);
		side3.setTextureOffset(22, 52).addBox(11.0F, -35.0F, -7.0F, 0.0F, 2.0F, 14.0F, 0.0F, false);

		left4 = new ModelRenderer(this);
		left4.setRotationPoint(0.0F, 0.0F, 0.25F);
		side3.addChild(left4);
		left4.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left4.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		left4.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(36, 88).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(36, 88).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		left4.setTextureOffset(36, 88).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window7 = new ModelRenderer(this);
		window7.setRotationPoint(-0.25F, 0.0F, 0.0F);
		left4.addChild(window7);
		

		frame7 = new ModelRenderer(this);
		frame7.setRotationPoint(0.0F, 0.0F, 0.0F);
		window7.addChild(frame7);
		frame7.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow8 = new ModelRenderer(this);
		glow8.setRotationPoint(0.0F, 0.0F, 0.0F);
		window7.addChild(glow8);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow8.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		right4 = new ModelRenderer(this);
		right4.setRotationPoint(0.0F, 0.0F, -8.25F);
		side3.addChild(right4);
		right4.setTextureOffset(92, 6).addBox(8.0F, -3.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(86, 86).addBox(8.0F, -32.0F, 6.75F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right4.setTextureOffset(16, 87).addBox(8.0F, -32.0F, 0.25F, 1.0F, 31.0F, 1.0F, 0.0F, false);
		right4.setTextureOffset(91, 25).addBox(8.0F, -32.0F, 1.0F, 1.0F, 2.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(99, 21).addBox(8.0F, -10.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(99, 21).addBox(8.0F, -17.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(98, 92).addBox(8.0F, -24.0F, 1.0F, 1.0F, 1.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(36, 82).addBox(8.75F, -9.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(36, 82).addBox(8.75F, -16.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);
		right4.setTextureOffset(36, 82).addBox(8.75F, -23.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		window8 = new ModelRenderer(this);
		window8.setRotationPoint(-0.25F, 0.0F, 0.0F);
		right4.addChild(window8);
		

		frame8 = new ModelRenderer(this);
		frame8.setRotationPoint(0.0F, 0.0F, 0.0F);
		window8.addChild(frame8);
		frame8.setTextureOffset(0, 72).addBox(9.0F, -30.0F, 1.0F, 0.0F, 6.0F, 6.0F, 0.0F, false);

		glow10 = new ModelRenderer(this);
		glow10.setRotationPoint(0.0F, 0.0F, 0.0F);
		window8.addChild(glow10);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -29.5F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.75F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 1.92F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.25F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 5.08F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.415F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 37).addBox(9.125F, -26.75F, 3.585F, 0.0F, 2.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -28.25F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.75F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 1.92F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.25F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 5.08F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.415F, 0.0F, 1.0F, 1.0F, 0.0F, false);
		glow10.setTextureOffset(63, 38).addBox(9.125F, -25.5F, 3.585F, 0.0F, 1.0F, 1.0F, 0.0F, false);

		roof = new ModelRenderer(this);
		roof.setRotationPoint(0.0F, 24.75F, 0.0F);
		roof.setTextureOffset(0, 24).addBox(-9.5F, -38.25F, -9.5F, 19.0F, 4.0F, 19.0F, 0.0F, false);
		roof.setTextureOffset(2, 48).addBox(-8.5F, -39.25F, -8.5F, 17.0F, 1.0F, 17.0F, 0.0F, false);
		roof.setTextureOffset(56, 48).addBox(-7.5F, -40.25F, -7.5F, 15.0F, 1.0F, 15.0F, 0.0F, false);

		lamp = new ModelRenderer(this);
		lamp.setRotationPoint(0.0F, 24.0F, 0.0F);
		lamp.setTextureOffset(8, 0).addBox(-1.5F, -40.0F, -1.5F, 3.0F, 1.0F, 3.0F, 0.0F, false);
		lamp.setTextureOffset(0, 0).addBox(-0.5F, -45.0F, -0.5F, 1.0F, 1.0F, 1.0F, 0.0F, false);
		lamp.setTextureOffset(75, 2).addBox(-1.0F, -44.0F, -1.25F, 2.0F, 4.0F, 0.0F, 0.0F, false);
		lamp.setTextureOffset(70, 2).addBox(-1.0F, -44.0F, 1.25F, 2.0F, 4.0F, 0.0F, 0.0F, false);
		lamp.setTextureOffset(70, 0).addBox(1.25F, -44.0F, -1.0F, 0.0F, 4.0F, 2.0F, 0.0F, false);
		lamp.setTextureOffset(75, 0).addBox(-1.25F, -44.0F, -1.0F, 0.0F, 4.0F, 2.0F, 0.0F, false);

		glow9 = new ModelRenderer(this);
		glow9.setRotationPoint(0.0F, 0.0F, 0.0F);
		lamp.addChild(glow9);
		glow9.setTextureOffset(14, 4).addBox(-1.0F, -44.5F, -1.0F, 2.0F, 5.0F, 2.0F, 0.0F, false);

		boti = new ModelRenderer(this);
		boti.setRotationPoint(1.0F, 22.0F, -8.75F);
		boti.setTextureOffset(115, 119).addBox(-9.0F, -32.0F, 1.0F, 16.0F, 32.0F, 0.0F, 0.0F, false);
		boti.setTextureOffset(115, 119).addBox(-9.0F, -32.0F, 16.5F, 16.0F, 32.0F, 0.0F, 0.0F, false);

		backing = new ModelRenderer(this);
		backing.setRotationPoint(0.0F, 24.0F, 0.0F);
		backing.setTextureOffset(115, 119).addBox(7.75F, -34.0F, -8.0F, 0.0F, 32.0F, 16.0F, 0.0F, false);
		backing.setTextureOffset(115, 119).addBox(-7.75F, -34.0F, -8.0F, 0.0F, 32.0F, 16.0F, 0.0F, false);
		
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		base.render(matrixStack, buffer, packedLight, packedOverlay);
		doors.render(matrixStack, buffer, packedLight, packedOverlay);
		side.render(matrixStack, buffer, packedLight, packedOverlay);
		side2.render(matrixStack, buffer, packedLight, packedOverlay);
		side3.render(matrixStack, buffer, packedLight, packedOverlay);
		roof.render(matrixStack, buffer, packedLight, packedOverlay);
		lamp.render(matrixStack, buffer, packedLight, packedOverlay);
		boti.render(matrixStack, buffer, packedLight, packedOverlay);
		backing.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		 EnumDoorState state = tile.getOpen();
	        switch (state) {
	            case ONE:
	                this.left2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE)); //Only open right door, left door is closed by default

	                this.right2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));

	                break;


	            case BOTH:
	                this.left2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.ONE));

	             
	                this.right2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.BOTH)); //Open left door,Right door is already open

	                break;


	            case CLOSED://close both doors
	                this.left2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
	                this.right2.rotateAngleY = (float) Math.toRadians(
	                        IDoorType.EnumDoorType.MODERN_POLICE_BOX.getRotationForState(EnumDoorState.CLOSED));
	                break;
	            default:
	                break;
	        }
	        base.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        doors.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        side.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        side2.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        side3.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        roof.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        lamp.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        //boti.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        //backing.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	        this.backing.showModel = false;

	        LIGHT = tile.lightLevel;

	        if (tile.getMatterState() != EnumMatterState.SOLID)
	            ALPHA = tile.alpha;
	        else ALPHA = 1F;
	        
	        if(tile.getBotiWorld() != null) {
    			PortalInfo info = new PortalInfo();
    			info.setPosition(tile.getPos());
    			info.setWorldShell(tile.getBotiWorld());
    			info.setTranslate(matrix -> {
					matrix.translate(-0.5, 1.1, -0.5);
    				ExteriorRenderer.applyTransforms(matrix, tile);
    			});
    			info.setTranslatePortal(matrix -> {
					matrix.translate(0, 1, 0);
    				matrix.rotate(Vector3f.XP.rotationDegrees(180));
    				matrix.rotate(Vector3f.YP.rotationDegrees(180));
    				matrix.rotate(Vector3f.YP.rotationDegrees(WorldHelper.getAngleFromFacing(tile.getBotiWorld().getPortalDirection())));
					matrix.translate(-0.5, -0.5, -0.5);
    			});
    			
    			info.setRenderPortal((matrix, buf) -> {
    				matrix.push();
    				matrix.scale(1.1F, 1.1F, 1.1F);
    				this.boti.render(matrix, buf.getBuffer(RenderType.getEntityCutout(ModernPoliceBoxExteriorRenderer.TEXTURE)), packedLight, packedOverlay);
    				matrix.pop();
    			});
    			
    			info.setRenderDoor((matrix, buf) -> {
    				matrix.push();
    				matrix.scale(1.1F, 1.1F, 1.1F);
    				this.doors.render(matrix, buf.getBuffer(RenderType.getEntityCutout(ModernPoliceBoxExteriorRenderer.TEXTURE)), packedLight, packedOverlay);
    				matrix.pop();
    			});
    			
    			BOTIRenderer.addPortal(info);
	        }
		
	}
}