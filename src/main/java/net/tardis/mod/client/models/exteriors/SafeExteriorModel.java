package net.tardis.mod.client.models.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.renderer.model.ModelRenderer;
import net.minecraft.entity.Entity;
import net.tardis.mod.misc.IDoorType.EnumDoorType;
//import net.tardis.mod.misc.IDoorType.EnumDoorType;
import net.tardis.mod.tileentities.exteriors.ExteriorTile;

// Made with Blockbench 3.7.4
// Exported for Minecraft version 1.15
// Paste this class into your mod and generate all required imports


public class SafeExteriorModel extends ExteriorModel {
	private final ModelRenderer BOTI;
	private final ModelRenderer door;
	private final ModelRenderer bb_main;

	public SafeExteriorModel() {
		textureWidth = 128;
		textureHeight = 128;

		BOTI = new ModelRenderer(this);
		BOTI.setRotationPoint(0.0F, 24.0F, 0.0F);
		BOTI.setTextureOffset(79, 1).addBox(-8.0F, -34.0F, -6.0F, 16.0F, 34.0F, 1.0F, -0.025F, false);

		door = new ModelRenderer(this);
		door.setRotationPoint(7.0F, 7.0F, -6.5F);
		door.setTextureOffset(30, 48).addBox(-14.0F, -16.0F, -0.5F, 14.0F, 32.0F, 1.0F, 0.0F, false);
		door.setTextureOffset(60, 26).addBox(-1.5F, -14.0F, -1.475F, 2.0F, 6.0F, 2.0F, 0.0F, false);
		door.setTextureOffset(60, 26).addBox(-1.5F, 8.0F, -1.475F, 2.0F, 6.0F, 2.0F, 0.0F, false);
		door.setTextureOffset(60, 34).addBox(-6.0F, -9.0F, -1.0F, 1.0F, 4.0F, 3.0F, 0.0F, false);
		door.setTextureOffset(60, 34).addBox(-6.0F, 5.0F, -1.0F, 1.0F, 4.0F, 3.0F, 0.0F, false);
		door.setTextureOffset(60, 10).addBox(-11.5F, -2.5F, -1.5F, 5.0F, 5.0F, 3.0F, 0.0F, false);
		door.setTextureOffset(60, 60).addBox(-14.0F, -8.5F, -0.475F, 14.0F, 3.0F, 2.0F, 0.0F, false);
		door.setTextureOffset(60, 60).addBox(-14.0F, 5.5F, -0.475F, 14.0F, 3.0F, 2.0F, 0.0F, false);

		bb_main = new ModelRenderer(this);
		bb_main.setRotationPoint(0.0F, 24.0F, 0.0F);
		bb_main.setTextureOffset(0, 0).addBox(-8.0F, -34.0F, -7.0F, 16.0F, 34.0F, 14.0F, 0.0F, false);
		bb_main.setTextureOffset(46, 0).addBox(-7.0F, -10.0F, 7.0F, 14.0F, 9.0F, 1.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 0).addBox(7.0F, -31.0F, -3.0F, 1.0F, 3.0F, 6.0F, 0.0F, false);
		bb_main.setTextureOffset(0, 0).addBox(-8.0F, -31.0F, -3.0F, 1.0F, 3.0F, 6.0F, 0.0F, true);
	}

	@Override
	public void setRotationAngles(Entity entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw, float headPitch){
		//previously the render function, render code was moved to a method below
	}

	@Override
	public void render(MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float red, float green, float blue, float alpha){
		BOTI.render(matrixStack, buffer, packedLight, packedOverlay);
		door.render(matrixStack, buffer, packedLight, packedOverlay);
		bb_main.render(matrixStack, buffer, packedLight, packedOverlay);
	}

	public void setRotationAngle(ModelRenderer modelRenderer, float x, float y, float z) {
		modelRenderer.rotateAngleX = x;
		modelRenderer.rotateAngleY = y;
		modelRenderer.rotateAngleZ = z;
	}

	@Override
	public void render(ExteriorTile tile, float scale, MatrixStack matrixStack, IVertexBuilder buffer, int packedLight, int packedOverlay, float alpha) {
		 matrixStack.push();
		 matrixStack.scale(1f, 1f, 1f);
		 matrixStack.translate(0, -0.75f, 0);
	     this.door.rotateAngleY = (float) Math.toRadians(EnumDoorType.SAFE.getRotationForState(tile.getOpen()));
	     door.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	     bb_main.render(matrixStack, buffer, packedLight, packedOverlay,1,1,1, alpha);
	     matrixStack.pop();
		
	}
}