package net.tardis.mod.client.guis;


import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.GuiContextBlock;
import net.tardis.mod.misc.GuiContext;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.UpdateTransductionMessage;

public class TransductionBarrierEdit extends Screen {

    private final TranslationTextComponent update_code = new TranslationTextComponent(Constants.Strings.GUI + "transduction.land_code.update");
    private final TranslationTextComponent title = new TranslationTextComponent(Constants.Strings.GUI + "transduction.title");
    private final TranslationTextComponent set_code_desc = new TranslationTextComponent(Constants.Strings.GUI + "transduction.land_code.desc");
    private final TranslationTextComponent set_code_suggestion = new TranslationTextComponent(Constants.Strings.GUI + "transduction.land_code.suggestion");
    private TextFieldWidget code;
    private BlockPos pos;
    private Button setCode;

    protected TransductionBarrierEdit(ITextComponent titleIn) {
        super(titleIn);
    }

    public TransductionBarrierEdit(GuiContext cont) {
        super(new StringTextComponent(""));
        this.pos = ((GuiContextBlock) cont).pos;
    }

    @Override
    protected void init() {
        super.init();
        this.buttons.clear();
        int w = this.width / 2, h = font.FONT_HEIGHT + 10;
        this.setCode = new Button(w - 40, height / 2 - h / 2 + 30, this.font.getStringWidth(update_code.getString()) + 10, 20, update_code, new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                Network.sendToServer(new UpdateTransductionMessage(pos, code.getText()));
                onClose();
            }
        });
        this.addButton(this.setCode);
        this.addButton(code = new TextFieldWidget(this.font, w - 100, height / 2 - h / 2, w, h, new TranslationTextComponent("")));
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        int w = this.width / 2;
        int h = this.height / 2;
        drawCenteredString(matrixStack, this.font, title.getString(), w + 10, h - 50, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, set_code_desc.getString(), w + 5, h - 30, 0xFFFFFF);
        if (this.code.isFocused()) {
            this.code.setSuggestion("");
        } else if (this.code.getText().isEmpty()) {
            this.code.setSuggestion(set_code_suggestion.getString());
        }
    }


    @Override
    public void onClose() {
        super.onClose();
    }

}
