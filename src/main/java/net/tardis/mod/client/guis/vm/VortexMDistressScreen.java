package net.tardis.mod.client.guis.vm;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import org.lwjgl.glfw.GLFW;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.widget.TextFieldWidget;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.client.guis.INeedTardisNames;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.VMDistressMessage;

public class VortexMDistressScreen extends VortexMFunctionScreen implements INeedTardisNames{

    private final TranslationTextComponent title = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.title");
    private final TranslationTextComponent selectTardis = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.select_tardis");
    private final TranslationTextComponent messageDesc = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.message");
    private final TranslationTextComponent userFound = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.tardis_found");
    private final TranslationTextComponent userNotFound = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.tardis_invalid");
    private final TranslationTextComponent sentText = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.send_signal");
    private final TranslationTextComponent checkNameText = new TranslationTextComponent(Constants.Strings.GUI_VM + "distress.check_name");
    private TextFieldWidget userInput;
    private TextFieldWidget messageBox;
    private Button send;
    private Button back;
    private Button increment;
    private Button decrement;
    private Button checkName;
    private int index = 0;
    private Map<ResourceLocation, String> names = Maps.newHashMap();
    private ArrayList<Entry<ResourceLocation, String>> nameList = Lists.newArrayList();
    private Entry<ResourceLocation, String> selectedTardis;
    private boolean isValid; //Used for rendering error messages on gui
    private boolean hasCheckedName; //Second flag to check if we should render error message

    public VortexMDistressScreen(ITextComponent title) {
        super(title);
    }

    public VortexMDistressScreen() {
    }

    @Override
    public void init() {
        super.init();
        String next = ">";
        String previous = "<";
        String sendButton = sentText.getString();
        String backButton = Constants.Translations.GUI_BACK.getString();
        String checkNames = checkNameText.getString();
        final int btnH = 20;

        userInput = new TextFieldWidget(this.font, this.getMinX() + 42, this.getMaxY() + 95, 60, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        messageBox = new TextFieldWidget(this.font, this.getMinX() + 165, this.getMaxY() + 115, 60, this.font.FONT_HEIGHT + 2, new TranslationTextComponent(""));
        send = new Button(this.getMinX() + 40, this.getMaxY() + 115, this.font.getStringWidth(sendButton) + 10, btnH, new TranslationTextComponent(sendButton), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                //Check if text box input has valid player.
                checkInputName();
                if (isValid) {
                    Network.sendToServer(new VMDistressMessage(selectedTardis.getKey(), messageBox.getText()));
                    closeScreen();
                }
            }
        });
        back = new Button(this.getMinX(), this.getMaxY() + 35, this.font.getStringWidth(backButton) + 8, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(backButton), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                ClientHelper.openGUI(Constants.Gui.VORTEX_MAIN, null);
            }
        });
        increment = new Button(this.getMinX() + 110, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(next), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                modIndex(1);
            }
        });
        decrement = new Button(this.getMinX() + 15, this.getMaxY() + 90, 20, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(previous), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                modIndex(-1);
            }
        });
        checkName = new Button(this.getMinX() + 165, this.getMaxY() + 50, 65, this.font.FONT_HEIGHT + 11, new TranslationTextComponent(checkNames), new Button.IPressable() {
            @Override
            public void onPress(Button button) {
                checkInputName();
            }
        });

        this.buttons.clear();
        this.addButton(userInput);
        this.addButton(send);
        this.addButton(increment);
        this.addButton(decrement);
        this.addButton(back);
        this.addButton(checkName);
        this.addButton(messageBox);
        userInput.setFocused2(true);
    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        drawCenteredString(matrixStack, this.font, title.getString(), this.getMinX() + 73, this.getMaxY() + 40, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, selectTardis.getString(), this.getMinX() + 75, this.getMaxY() + 76, 0xFFFFFF);
        drawCenteredString(matrixStack, this.font, messageDesc.getString(), this.getMinX() + 197, this.getMaxY() + 100, 0xFFFFFF);
        if (this.hasCheckedName) {
            if (!this.isValid && !this.userInput.isFocused()) {
                drawCenteredString(matrixStack, this.font, userNotFound.getString(), this.getMinX() + 197, this.getMaxY() + 75, 0xFF0000);
                String user = userInput.getText();
                drawCenteredString(matrixStack, this.font, userInput.getText().isEmpty() || userInput.getText() == null ? "Null" : user, this.getMinX() + 196, this.getMaxY() + 85, 0xffcc00);
            } else if (this.isValid && !this.userInput.getText().isEmpty() && !this.userInput.isFocused()) {
                drawCenteredString(matrixStack, this.font, userFound.getString(), this.getMinX() + 197, this.getMaxY() + 75, 0x00FF00);
            }
        }
    }

    @Override
    public boolean shouldCloseOnEsc() {
        return true;
    }

    @Override
    public void closeScreen() {
        super.closeScreen();
    }

    @Override
    public boolean isPauseScreen() {
        return true;
    }

    @Override
    public boolean keyPressed(int keyCode, int scanCode, int bitmaskModifier) {
        if (this.userInput.isFocused()) {
            if (keyCode == GLFW.GLFW_KEY_TAB) {
                modIndex(1);
            }
            super.keyPressed(keyCode, scanCode, bitmaskModifier);
            return true;
        }
        return super.keyPressed(keyCode, scanCode, bitmaskModifier);
    }

    @Override
    public boolean mouseClicked(double mouseX, double mouseY, int mouseZ) {
        if (!this.checkName.mouseClicked(mouseX, mouseY, mouseZ))
            this.hasCheckedName = false;
        return super.mouseClicked(mouseX, mouseY, mouseZ);
    }

    @Override
    public void renderBackground(MatrixStack matrixStack) {
        super.renderBackground(matrixStack);
    }

    @Override
    public int texWidth() {
        return super.texWidth();
    }

    @Override
    public int texHeight() {
        return super.texHeight();
    }

    @Override
    public int getMinY() {
        return super.getMinY();
    }

    @Override
    public int getMinX() {
        return super.getMinX();
    }

    @Override
    public int getMaxX() {
        return super.getMaxX();
    }

    @Override
    public int getMaxY() {
        return super.getMaxY();
    }

    public void modIndex(int amount) {
        int temp = index + amount;
        if (temp < nameList.size() && temp > 0) {
            this.index = temp;
        }
        if (temp > nameList.size() - 1) {
            this.index = 0;
        } else if (temp < 0) {
            this.index = nameList.size() - 1;
        }
        if (this.nameList.size() == 0) {
        	this.index = 0;
        }
        this.selectedTardis = this.nameList.get(index);
        this.userInput.setText(this.selectedTardis != null ? selectedTardis.getValue() : "");
        this.userInput.setCursorPositionZero();
    }

    /**
     * Used to check if textbox contents match an online player
     *
     * @implSpec Only use this for the Check name button
     */
    public void checkInputName() {
        //getPlayerInfo has one other overload. This overload method will not work for uuids, so we need this in a seperate method
        String name = this.userInput.getText();
        this.names.entrySet().forEach(entry -> {
        	if (entry.getValue().contentEquals(name)) {
        		this.isValid = true;
        		this.selectedTardis = entry;
        	}
        	else {
        		this.isValid = false;
        	}
        });
        this.hasCheckedName = true;
    }

	@Override
	public void setNamesFromServer(Map<ResourceLocation, String> nameMap) {
		this.names.clear();
		this.names.putAll(nameMap);
		this.names.entrySet().forEach(entry -> {
			this.nameList.add(entry);
		});
	}

}
