package net.tardis.mod.client.guis.widgets;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.audio.SimpleSound;
import net.minecraft.client.audio.SoundHandler;
import net.minecraft.client.gui.widget.button.Button;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.text.StringTextComponent;
import net.tardis.mod.sounds.TSounds;

public class ImageToggleButton extends Button{

	private boolean checked = false;
	private int u, v;
	
	public ImageToggleButton(int x, int y, int u, int v, int width, int height, IPressable pressedAction) {
		super(x, y, width, height, new StringTextComponent(""), pressedAction);
		this.u = u;
		this.v = v;
	}

	@Override
	public void renderWidget(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
		
		int offset = this.isChecked() ? height : 0;
		
		this.blit(matrixStack, x, y, u, v + offset, width, height);
		
	}
	
	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	@Override
	public void playDownSound(SoundHandler handler) {
		
		SoundEvent event = TSounds.SUBSYSTEMS_ON.get();
		
		if(this.isChecked())
			event = TSounds.SUBSYSTEMS_OFF.get();
		
		handler.play(SimpleSound.master(event, 1.0F));
	}

	public boolean isChecked() {
		return this.checked;
	}
}
