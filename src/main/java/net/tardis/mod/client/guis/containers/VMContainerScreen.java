package net.tardis.mod.client.guis.containers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.platform.GlStateManager;
import net.minecraft.client.gui.screen.inventory.ContainerScreen;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.ITextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.containers.VMContainer;
import net.tardis.mod.helper.PlayerHelper;

/**
 * Created by 50ap5ud5
 * on 5 May 2020 @ 7:35:30 pm
 */
public class VMContainerScreen extends ContainerScreen<VMContainer> {
    private static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/gui/containers/vm_container.png");
    private final int inventoryRows;

    public VMContainerScreen(VMContainer screenContainer, PlayerInventory inv, ITextComponent titleIn) {
        super(screenContainer, inv, titleIn);
        this.inventoryRows = screenContainer.getNumRows();
    }

    @Override
    protected void drawGuiContainerForegroundLayer(MatrixStack matrixStack, int mouseX, int mouseY) {
        this.font.drawString(matrixStack, this.title.getString(), 8.0F, (float) (this.ySize - 121), 4210752);
        this.font.drawString(matrixStack, this.playerInventory.getDisplayName().getString(), 8.0F, (float) (this.ySize - 92 + 2), 4210752);

    }

    @Override
    public void render(MatrixStack matrixStack, int mouseX, int mouseY, float partialTicks) {
        this.renderBackground(matrixStack);
        super.render(matrixStack, mouseX, mouseY, partialTicks);
        this.renderHoveredTooltip(matrixStack, mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(MatrixStack matrixStack, float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color4f(1.0F, 1.0F, 1.0F, 1.0F); //TODO Is this needed?
        this.minecraft.getTextureManager().bindTexture(TEXTURE);
        int i = (this.width - this.xSize) / 2;
        int j = (this.height - this.ySize) / 2;
        int offset = 40;
        this.blit(matrixStack, i, j + offset, 0, 0, this.xSize, this.inventoryRows * 18 + 17);
        this.blit(matrixStack, i, j + offset + this.inventoryRows * 18 + 17, 0, 126, this.xSize, 96);
    }

    @Override
    public void onClose() {
        super.onClose();
        PlayerHelper.closeVMModel(this.minecraft.player);
    }
}
