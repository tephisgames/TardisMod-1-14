package net.tardis.mod.client.renderers.entity;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.entity.TardisDisplayEntity;

public class TardisDisplayEntityRenderer extends EntityRenderer<TardisDisplayEntity>{

	public TardisDisplayEntityRenderer(EntityRendererManager renderManager) {
		super(renderManager);
	}

	@Override
	public ResourceLocation getEntityTexture(TardisDisplayEntity entity) {
		return null;
	}
	
	

	@Override
	public void render(TardisDisplayEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn,
	        IRenderTypeBuffer bufferIn, int packedLightIn) {
		matrixStackIn.push();
		if(entity.getTile() != null) {
			TileEntityRendererDispatcher.instance.renderTileEntity(entity.getTile(), partialTicks, matrixStackIn, bufferIn);
		}
		matrixStackIn.pop();
	}

}
