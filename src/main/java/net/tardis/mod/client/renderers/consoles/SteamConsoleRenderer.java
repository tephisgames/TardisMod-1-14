package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.SteamConsoleModel;
import net.tardis.mod.tileentities.consoles.SteamConsoleTile;

public class SteamConsoleRenderer extends TileEntityRenderer<SteamConsoleTile> {

	public static final SteamConsoleModel MODEL = new SteamConsoleModel(tex -> RenderType.getEntityCutout(tex));
	public static final ResourceLocation TEXURE = new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png");

	public SteamConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	
	@Override
	public void render(SteamConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
		matrixStackIn.push();
		matrixStackIn.translate(0.5, 0.5, 0.5); //Translate to middle of block
		matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(180));
		float scale = 0.3F;
		matrixStackIn.scale(scale, scale, scale);
		ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/steam.png");
		texture = console.getVariant() != null ? console.getVariant().getTexture() : TEXURE;
		MODEL.render(console, scale, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
		matrixStackIn.pop();
	}
}
