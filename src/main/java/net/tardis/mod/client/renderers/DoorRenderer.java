package net.tardis.mod.client.renderers;

import com.mojang.blaze3d.matrix.MatrixStack;
import com.mojang.blaze3d.vertex.IVertexBuilder;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.WorldRenderer;
import net.minecraft.client.renderer.entity.EntityRenderer;
import net.minecraft.client.renderer.entity.EntityRendererManager;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.client.models.interiordoors.IInteriorDoorRenderer;
import net.tardis.mod.client.models.interiordoors.TrunkInteriorModel;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.helper.PlayerHelper;
import net.tardis.mod.items.TItems;
import net.tardis.mod.misc.IDoorType;
import net.tardis.mod.misc.IDoorType.EnumDoorType;

public class DoorRenderer extends EntityRenderer<DoorEntity> {

    public static IInteriorDoorRenderer MODEL = new TrunkInteriorModel();

    public DoorRenderer(EntityRendererManager manager) {
        super(manager);
    }


    @Override
    public void render(DoorEntity entity, float entityYaw, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int packedLightIn) {
        matrixStackIn.push();
        applyTranslations(matrixStackIn, entity.rotationYaw - 180, entity.getHorizontalFacing());

        IDoorType type = EnumDoorType.TRUNK;
        if (entity.doorType != null)
            type = entity.doorType;

        ResourceLocation texture = type.getInteriorDoorRenderer().getTexture();
        type.getInteriorDoorRenderer().render(entity, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture, true)), packedLightIn, OverlayTexture.NO_OVERLAY);

        matrixStackIn.pop();

        if (PlayerHelper.InEitherHand(Minecraft.getInstance().player, stack -> stack.getItem() == TItems.DEBUG.get())) {
            matrixStackIn.push();
            IRenderTypeBuffer.Impl buffer = Minecraft.getInstance().getRenderTypeBuffers().getBufferSource();
            IVertexBuilder builder = buffer.getBuffer(RenderType.getLines());
            WorldRenderer.drawBoundingBox(matrixStackIn, builder, entity.getDoorBB(), 0.4F, 1, 0.4F, 0.4F);
            matrixStackIn.pop();
        }
    }
    
    public static void applyTranslations(MatrixStack matrix, float rotation, Direction facing) {
    	matrix.translate(0, 1.5, 0);
    	matrix.rotate(Vector3f.YP.rotationDegrees(rotation));
        if (facing == Direction.EAST || facing == Direction.WEST)
        	matrix.rotate(Vector3f.YP.rotationDegrees(180));
        matrix.rotate(Vector3f.ZP.rotationDegrees(180));
        matrix.translate(0, 0, 0.1);
    }

    @Override
    public ResourceLocation getEntityTexture(DoorEntity entity) {
        return null;
    }


}
