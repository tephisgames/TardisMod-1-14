package net.tardis.mod.client.renderers.consoles;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.model.ItemCameraTransforms.TransformType;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.client.renderer.tileentity.TileEntityRenderer;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.consoles.CoralConsoleModel;
import net.tardis.mod.controls.MonitorControl;
import net.tardis.mod.helper.Helper;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;

public class CoralConsoleRenderer extends TileEntityRenderer<CoralConsoleTile> {

    public static final WorldText TEXT = new WorldText(0.31F, 0.26F, 0.003F, 0xFFFFFF);
	public static CoralConsoleModel model = new CoralConsoleModel();
	
    public CoralConsoleRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}
    
    
    @Override
    public void render(CoralConsoleTile console, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn) {
        
    	matrixStackIn.push();
		matrixStackIn.translate(0.5, 1.125, 0.5); //Translate to middle of block
		matrixStackIn.rotate(Vector3f.ZN.rotationDegrees(180));
        
        //Monitor Text
        console.getControl(MonitorControl.class).ifPresent(monitor -> {

			matrixStackIn.push();
			matrixStackIn.rotate(Vector3f.YP.rotationDegrees(150));
			matrixStackIn.translate(-0.15, -0.32, -10.86 / 16.0);
			TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, Helper.getConsoleText(console));
			matrixStackIn.pop();
        });
        ResourceLocation texture = new ResourceLocation(Tardis.MODID, "textures/consoles/coral.png");
        if(console.getVariant() != null) 
        	texture = console.getVariant().getTexture();
		
		float scale = 0.75F;
        matrixStackIn.scale(scale, scale, scale);
        model.render(console, scale, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityTranslucent(texture)), combinedLightIn, OverlayTexture.NO_OVERLAY, 1, 1, 1, 1.0F);
        
        //Sonic
        matrixStackIn.push();
        matrixStackIn.translate(0.225, 0.5, 1.8);
        matrixStackIn.rotate(Vector3f.XN.rotationDegrees(15));
        float sonic_scale = 0.75F;
        matrixStackIn.scale(sonic_scale, sonic_scale, sonic_scale);
        Minecraft.getInstance().getItemRenderer().renderItem(console.getSonicItem(), TransformType.NONE, combinedLightIn, combinedOverlayIn, matrixStackIn, bufferIn);
        matrixStackIn.pop();

        matrixStackIn.pop();
    }

}       