package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;
import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.SafeExteriorModel;
import net.tardis.mod.tileentities.exteriors.SafeExteriorTile;

public class SafeExteriorRenderer extends ExteriorRenderer<SafeExteriorTile> {

	public SafeExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}

	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, "textures/exteriors/safe.png");
	public static final SafeExteriorModel MODEL = new SafeExteriorModel();
	
	@Override
	public void renderExterior(SafeExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.translate(0, -0.26, 0);
		MODEL.render(tile, 1.0F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
	}

}
