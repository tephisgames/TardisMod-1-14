package net.tardis.mod.client.renderers.exteriors;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.renderer.IRenderTypeBuffer;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.tileentity.TileEntityRendererDispatcher;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.vector.Vector3f;
import net.tardis.mod.Tardis;
import net.tardis.mod.client.models.exteriors.ModernPoliceBoxExteriorModel;
import net.tardis.mod.misc.WorldText;
import net.tardis.mod.tileentities.exteriors.ModernPoliceBoxExteriorTile;

/**
 * Created by 50ap5ud5
 * on 18 Apr 2020 @ 12:55:35 pm
 */
public class ModernPoliceBoxExteriorRenderer extends ExteriorRenderer<ModernPoliceBoxExteriorTile>{
	
//	private static BotiManager boti = new BotiManager();
	
	public static final ResourceLocation TEXTURE = new ResourceLocation(Tardis.MODID, 
			"textures/exteriors/modern_police_box_alt.png");
	public static final WorldText TEXT = new WorldText(1.1F, 0.125F, 0.015F, 0xFFFFFF);
	
	private ModernPoliceBoxExteriorModel model = new ModernPoliceBoxExteriorModel();
	
	public ModernPoliceBoxExteriorRenderer(TileEntityRendererDispatcher rendererDispatcherIn) {
		super(rendererDispatcherIn);
	}
	
	@Override
	public void renderExterior(ModernPoliceBoxExteriorTile tile, float partialTicks, MatrixStack matrixStackIn, IRenderTypeBuffer bufferIn, int combinedLightIn, int combinedOverlayIn, float alpha) {
		matrixStackIn.push();
		/*We want to shift the render up in this case 
		so that when we place the exterior two blocks up from the ground,
		the base of the exterior will be rendered as standing on the ground
		*/
		matrixStackIn.translate(0, -1.15, 0); // Translation must be negative as models are loaded in upside down.
		matrixStackIn.scale(1.1f, 1.1f, 1.1f); //Scales the model down by 4
		this.model.render(tile, 1.1F, matrixStackIn, bufferIn.getBuffer(RenderType.getEntityCutoutNoCull(TEXTURE)), combinedLightIn, combinedOverlayIn, alpha);
		matrixStackIn.pop();
		
		
		
		//Front
		matrixStackIn.push();
		matrixStackIn.translate(-0.45, -1.96, -12.175 / 16.0F);
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
		matrixStackIn.pop();
		
		//Left text
		matrixStackIn.push();
		matrixStackIn.translate(-0.77, -1.96, 7.25 / 16.0F);
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(90));
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
		matrixStackIn.pop();
        
        //Right text
		matrixStackIn.push();
		matrixStackIn.translate(0.77, -1.96, -7.25 / 16.0F);
		matrixStackIn.rotate(Vector3f.YN.rotationDegrees(90));
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
		matrixStackIn.pop();
        
        //BACK text
		matrixStackIn.push();
		matrixStackIn.translate(0.45, -1.96, 12.175 / 16.0F);
		matrixStackIn.rotate(Vector3f.YP.rotationDegrees(180));
		TEXT.renderText(matrixStackIn, bufferIn, combinedLightIn, tile.getCustomName());
		matrixStackIn.pop();
		
	}

}
