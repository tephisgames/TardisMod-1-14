package net.tardis.mod.trades;

import java.util.function.Supplier;

import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.village.PointOfInterestType;
import net.minecraftforge.fml.RegistryObject;
import net.minecraftforge.registries.DeferredRegister;
import net.minecraftforge.registries.ForgeRegistries;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;

public class TPointOfInterest {
	
	public static DeferredRegister<PointOfInterestType> POINT_OF_INTERESTS = DeferredRegister.create(ForgeRegistries.POI_TYPES, Tardis.MODID);
	
	public static RegistryObject<PointOfInterestType> TELESCOPE = POINT_OF_INTERESTS.register("story_teller", () -> registerPOI("story_teller", () -> TBlocks.telescope.get()));
	
	
	public static PointOfInterestType registerPOI(String name, Supplier<Block> block) {
		return new PointOfInterestType("tardis:" + name, 
				ImmutableSet.copyOf(block.get().getStateContainer().getValidStates()),
				1, 1);
	}

}
