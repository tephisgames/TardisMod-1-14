package net.tardis.mod.tileentities.machines;

import net.minecraft.tileentity.ITickableTileEntity;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraft.util.Direction;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.LazyOptional;
import net.minecraftforge.energy.CapabilityEnergy;
import net.minecraftforge.energy.EnergyStorage;
import net.minecraftforge.energy.IEnergyStorage;
import net.tardis.mod.cap.Capabilities;
import net.tardis.mod.cap.ITardisWorldData;
import net.tardis.mod.tileentities.TTiles;

/**
 * Forge Energy transferring machine
 * Originally named Artron Tap (misleading)
 * <br> 1.16: Refactor this to Roundel Tap as originally intended in 1.14
 * <br> Because we were very close to releasing 1.3, the registry name could not be changed
 *
 */
public class RoundelTapTile extends TileEntity implements IEnergyStorage, ITickableTileEntity{

	public RoundelTapTile(TileEntityType<?> tileEntityTypeIn) {
		super(tileEntityTypeIn);
	}
	
	public RoundelTapTile() {
		super(TTiles.ROUNDEL_TAP.get());
	}

	@SuppressWarnings("unchecked")
	@Override
	public <T> LazyOptional<T> getCapability(Capability<T> cap, Direction side) {
		return cap == CapabilityEnergy.ENERGY ? LazyOptional.of(() -> (T)this) : super.getCapability(cap, side);
	}

	@Override
	public int receiveEnergy(int maxReceive, boolean simulate) {
		return this.getEnergy().receiveEnergy(maxReceive, simulate);
	}

	@Override
	public int extractEnergy(int maxExtract, boolean simulate) {
		return this.getEnergy().extractEnergy(maxExtract, simulate);
	}

	@Override
	public int getEnergyStored() {
		return this.getEnergy().getEnergyStored();
	}

	@Override
	public int getMaxEnergyStored() {
		return this.getEnergy().getMaxEnergyStored();
	}

	@Override
	public boolean canExtract() {
		return this.getEnergy().canExtract();
	}

	@Override
	public boolean canReceive() {
		return this.getEnergy().canReceive();
	}
	
	public EnergyStorage getEnergy() {
		ITardisWorldData cap = world.getCapability(Capabilities.TARDIS_DATA).orElse(null);
		if(cap != null)
			return cap.getEnergy();
		return new EnergyStorage(0);
	}

	@Override
	public void tick() {
		this.pushPower();
	}

	public void pushPower() {
		for(Direction dir : Direction.values()) {
			TileEntity te = world.getTileEntity(getPos().offset(dir));
			if(te != null)
				te.getCapability(CapabilityEnergy.ENERGY, dir.getOpposite()).ifPresent(cap -> {
					int power = this.getEnergyStored();
					int accepted = cap.receiveEnergy(power, false);
					this.extractEnergy(accepted, false);
				});
		}
	}

}
