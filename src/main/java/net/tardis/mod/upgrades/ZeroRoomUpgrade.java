package net.tardis.mod.upgrades;

import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.potion.EffectInstance;
import net.minecraft.potion.Effects;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class ZeroRoomUpgrade extends Upgrade{

	public ZeroRoomUpgrade(UpgradeEntry entry, ConsoleTile tile, Class<? extends Subsystem> clazz) {
		super(entry, tile, clazz);
		tile.registerTicker(console -> {
			if(!console.getWorld().isRemote && this.isUsable() && console.getWorld().getGameTime() % 20 == 0) {
				for(PlayerEntity player : console.getWorld().getPlayers()) {
					
					EffectInstance posion = player.getActivePotionEffect(Effects.POISON);
					if(posion != null)
						player.removePotionEffect(Effects.POISON);
					
					if(player.getHealth() < player.getMaxHealth()) {
						player.heal(1F);
						this.damage(1, DamageType.ITEM, null);
					}
				}
			}
		});
	}

	@Override
	public void onLand() {}

	@Override
	public void onTakeoff() {}

	@Override
	public void onFlightSecond() {}

}
