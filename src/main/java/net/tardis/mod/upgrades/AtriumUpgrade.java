package net.tardis.mod.upgrades;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.annotation.Nullable;

import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.nbt.INBT;
import net.minecraft.nbt.ListNBT;
import net.minecraft.nbt.NBTUtil;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.blocks.TBlocks;
import net.tardis.mod.registries.ExteriorRegistry;
import net.tardis.mod.subsystem.Subsystem;
import net.tardis.mod.tileentities.ConsoleTile;

public class AtriumUpgrade extends Upgrade implements INBTSerializable<CompoundNBT>{

	private static ResourceLocation REGISTRY_NAME = new ResourceLocation(Tardis.MODID, "atrium");
	public static int radius = 5;
	private Map<BlockPos, BlockState> blocks = new HashMap<BlockPos, BlockState>();
	private int width = 0;
	private int height = 0;
	
	public AtriumUpgrade(UpgradeEntry entry, ConsoleTile tile, @Nullable Class<? extends Subsystem> sys) {
		super(entry, tile, sys);
		tile.registerDataHandler(REGISTRY_NAME, this);
	}
	
	//Should be relative to the exterior
	public void add(BlockPos pos, BlockState state) {
		this.blocks.put(pos, state);
	}
	
	public BlockState getState(BlockPos pos) {
		return this.blocks.getOrDefault(pos, Blocks.AIR.getDefaultState());
	}
	
	public Map<BlockPos, BlockState> getStoredBlocks(){
		return this.blocks;
	}
	
	public boolean isActive() {
		if(this.getConsole() != null && this.getConsole().getExteriorType() == ExteriorRegistry.DISGUISE.get())
			return false;
		return this.isUsable();
	}
	
	public void recalculateSize() {
		int width = 0;
		int height = 0;
		for(BlockPos pos : this.blocks.keySet()) {
			if(Math.abs(pos.getX()) > width)
				width = Math.abs(pos.getX());
			if(Math.abs(pos.getZ()) > width)
				width = Math.abs(pos.getZ());
			
			if(pos.getY() > height)
				height = pos.getY();
		}
		this.width = width;
		this.height = height;
	}
	
	public int getWidth() {
		return this.width;
	}
	
	public int getHeight() {
		return this.height;
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		ListNBT list = new ListNBT();
		for(Entry<BlockPos, BlockState> entry : this.blocks.entrySet()) {
			CompoundNBT nbt = new CompoundNBT();
			nbt.putLong("pos", entry.getKey().toLong());
			nbt.put("state", NBTUtil.writeBlockState(entry.getValue()));
			list.add(nbt);
		}
		tag.put("list", list);
		return tag;
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {
		ListNBT list = nbt.getList("list", Constants.NBT.TAG_COMPOUND);
		for(INBT item : list) {
			CompoundNBT comp = (CompoundNBT)item;
			this.blocks.put(BlockPos.fromLong(comp.getLong("pos")), NBTUtil.readBlockState(nbt.getCompound("state")));
		}
	}

	@Override
	public void onLand() {
		ServerWorld world = this.getConsole().getWorld().getServer().getWorld(this.getConsole().getDestinationDimension());
		if(world != null) {
			Set<Entry<BlockPos, BlockState>> states = this.getStoredBlocks().entrySet();
			for(Entry<BlockPos, BlockState> first : states) {
				System.out.println("Console's Destination: " + this.getConsole().getDestinationPosition());
				System.out.println("Atrium Placement: " + this.getConsole().getDestinationPosition().add(first.getKey()));
				if(first.getValue().isSolid())
					world.setBlockState(this.getConsole().getDestinationPosition().add(first.getKey()), first.getValue(), 2);
			}
			
			for(Entry<BlockPos, BlockState> first : states) {
				if(!first.getValue().isSolid())
					world.setBlockState(this.getConsole().getDestinationPosition().add(first.getKey()), first.getValue(), 2);
			}
			this.getStoredBlocks().clear();
		}
	}

	@Override
	public void onTakeoff() {
		if(this.isActive()) {
			ServerWorld world = this.getConsole().getWorld().getServer().getWorld(this.getConsole().getCurrentDimension());
			if(world != null) {
				int extX = this.getConsole().getCurrentLocation().down().getX(), extY = this.getConsole().getCurrentLocation().down().getY(), extZ = this.getConsole().getCurrentLocation().down().getZ();
				for(int x = extX - AtriumUpgrade.radius; x < extX + AtriumUpgrade.radius; ++x) {
					for(int z = extZ - AtriumUpgrade.radius; z < extZ + AtriumUpgrade.radius; ++z) {
						if(world.getBlockState(new BlockPos(x, extY, z)).getBlock() == TBlocks.atrium_block.get()) {
							for(int y = extY; y < extY + 11; ++y) {
								BlockPos pos = new BlockPos(x, y, z);
								BlockState state = world.getBlockState(pos);
								if(!state.hasTileEntity() && !state.getMaterial().isReplaceable() && !state.getMaterial().isLiquid()) {
									this.add(pos.subtract(this.getConsole().getCurrentLocation()), state);
									world.setBlockState(pos, Blocks.AIR.getDefaultState(), 18);
								}
							}
						}
					}
				}
				this.recalculateSize();
			}
		}
	}

	@Override
	public void onFlightSecond() {}
}
