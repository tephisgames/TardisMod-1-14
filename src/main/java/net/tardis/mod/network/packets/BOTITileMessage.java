package net.tardis.mod.network.packets;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.boti.stores.TileStore;
import net.tardis.mod.client.ClientPacketHandler;

import javax.annotation.Nullable;
import java.util.function.Supplier;

public class BOTITileMessage {

    public BlockPos pos;
    public TileStore store;

    public BOTITileMessage(@Nullable BlockPos pos, TileStore store){
        this(store);
        this.pos = pos;
    }

    public BOTITileMessage(TileStore store){
        this.store = store;
    }

    public static void encode(BOTITileMessage mes, PacketBuffer buf){
        boolean hasPos = mes.pos != null;
        buf.writeBoolean(hasPos);
        if(hasPos)
            buf.writeBlockPos(mes.pos);
        mes.store.encode(buf);
    }

    public static BOTITileMessage decode(PacketBuffer buf){

        if(buf.readBoolean())
            return new BOTITileMessage(buf.readBlockPos(), TileStore.decode(buf));

        return new BOTITileMessage(TileStore.decode(buf));
    }

    public static void handle(BOTITileMessage mes, Supplier<NetworkEvent.Context> context){
        context.get().enqueueWork(() -> ClientPacketHandler.handleBotiTileMessage(mes));
        context.get().setPacketHandled(true);
    }


}
