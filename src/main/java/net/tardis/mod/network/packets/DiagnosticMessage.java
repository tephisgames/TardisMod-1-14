package net.tardis.mod.network.packets;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

import com.google.common.collect.Lists;

import net.minecraft.network.PacketBuffer;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraftforge.fml.network.NetworkEvent;
import net.tardis.mod.client.ClientHelper;
import net.tardis.mod.constants.Constants;
import net.tardis.mod.contexts.gui.GuiContextSubsytem;
import net.tardis.mod.misc.SpaceTimeCoord;
import net.tardis.mod.subsystem.SubsystemInfo;
import net.tardis.mod.tileentities.console.misc.ArtronUse;

public class DiagnosticMessage {
	
	List<SubsystemInfo> data;
	SpaceTimeCoord location;
	List<ArtronUseInfo> uses;
	
	public DiagnosticMessage(List<SubsystemInfo> data, SpaceTimeCoord location, List<ArtronUseInfo> uses) {
		this.data = data;
		this.location = location;
		this.uses = uses;
	}
	
	public static void encode(DiagnosticMessage mes, PacketBuffer buf) {
		buf.writeInt(mes.data.size());
		
		for(SubsystemInfo info : mes.data) {
			buf.writeCompoundTag(info.serializeNBT());
		}
		
		buf.writeCompoundTag(mes.location.serialize());
		
		buf.writeInt(mes.uses.size());
		for(ArtronUseInfo use : mes.uses) {
			use.encode(buf);
		}
		
	}
	
	public static DiagnosticMessage decode(PacketBuffer buf) {
		
		List<SubsystemInfo> list = new ArrayList<>();
		
		int size = buf.readInt();
		
		for(int i = 0; i < size; ++i) {
			list.add(new SubsystemInfo(buf.readCompoundTag()));
		}
		
		SpaceTimeCoord coord = SpaceTimeCoord.deserialize(buf.readCompoundTag());
		
		List<ArtronUseInfo> uses = Lists.newArrayList();
		int useSize = buf.readInt();
		for(int i = 0; i < useSize; ++i) {
			uses.add(ArtronUseInfo.decode(buf));
		}
		
		return new DiagnosticMessage(list, coord, uses);
		
	}
	
	public static void handle(DiagnosticMessage mes, Supplier<NetworkEvent.Context> context) {
		context.get().enqueueWork(() -> {
			ClientHelper.openGUI(Constants.Gui.DIAGNOSTIC, new GuiContextSubsytem(mes.data, mes.location, mes.uses));
		});
		context.get().setPacketHandled(true);
	}
	
	public static class ArtronUseInfo{
		public TranslationTextComponent key;
		public float use;
		
		public ArtronUseInfo(ArtronUse use) {
			this(use.getType().getTranslation().getKey(), use.getArtronUsePerTick());
		}
		
		public ArtronUseInfo(String key, float use) {
			this.key = new TranslationTextComponent(key);
			this.use = use;
		}
		
		public void encode(PacketBuffer buf) {
			int size = this.key.getKey().length();
			buf.writeInt(size);
			buf.writeString(this.key.getKey(), size);
			buf.writeFloat(this.use);
		}
		
		public static ArtronUseInfo decode(PacketBuffer buf){
			int size = buf.readInt();
			String key = buf.readString(size);
			float use = buf.readFloat();
			return new ArtronUseInfo(key, use);
		}
	}

}
