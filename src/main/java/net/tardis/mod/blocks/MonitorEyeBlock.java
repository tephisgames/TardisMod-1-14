package net.tardis.mod.blocks;

import net.minecraft.block.BlockState;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Rotation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.shapes.IBooleanFunction;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.util.math.shapes.VoxelShapes;
import net.minecraft.world.IBlockReader;
import net.tardis.mod.helper.VoxelShapeUtils;

public class MonitorEyeBlock extends MonitorBlock {

    public static final VoxelShape NORTH = createVoxelShape();
    public static final VoxelShape EAST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_90);
    public static final VoxelShape SOUTH = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.CLOCKWISE_180);
    public static final VoxelShape WEST = VoxelShapeUtils.rotate(createVoxelShape(), Rotation.COUNTERCLOCKWISE_90);

    public MonitorEyeBlock(Properties prop, int guiID, double x, double y, double z, double width, double height) {
        super(prop, guiID, x, y, z, width, height);
    }

    public static VoxelShape createVoxelShape() {
        VoxelShape shape = VoxelShapes.create(-0.125, 0.0625, 0.890625, 0.0625, 0.9375, 1.0);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.0625, 0.890625, 0.9375, 0.1875, 1.0), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.8125, 0.890625, 0.9375, 0.9375, 1.0), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.0625, 0.1875, 0.9375, 0.9375, 0.8125, 1.0), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(0.9375, 0.0625, 0.890625, 1.125, 0.9375, 1.0), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(-0.375, 0.3125, 0.890625, -0.125, 0.6875, 1.0), IBooleanFunction.OR);
        shape = VoxelShapes.combineAndSimplify(shape, VoxelShapes.create(1.125, 0.3125, 0.890625, 1.375, 0.6875, 1.0), IBooleanFunction.OR);
        return shape;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader worldIn, BlockPos pos, ISelectionContext context) {
        switch (state.get(BlockStateProperties.HORIZONTAL_FACING)) {
            case EAST:
                return EAST;
            case SOUTH:
                return SOUTH;
            case WEST:
                return WEST;
            default:
                return NORTH;
        }
    }

}
