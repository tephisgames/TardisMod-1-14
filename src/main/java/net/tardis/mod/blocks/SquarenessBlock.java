package net.tardis.mod.blocks;

import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;

/**
 * Created by Swirtzly
 * on 15/04/2020 @ 11:51
 */
public class SquarenessBlock extends TileBlock {
    public SquarenessBlock(Properties prop) {
        super(prop);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.INVISIBLE;
    }
}
