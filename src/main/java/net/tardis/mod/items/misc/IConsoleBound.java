package net.tardis.mod.items.misc;

import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
/** Interface that allows subsequent objects (mainly {@linkplain ItemStack}(s)) to be able to receive Tardis Names and Tardis World Keys*/
public interface IConsoleBound {

    /** Get the Tardis' World Key ResourceLocation.
     * <br> This is the true unique instance of the Tardis' ID*/
	ResourceLocation getTardis(ItemStack stack);

	void setTardis(ItemStack stack, ResourceLocation world);
	
	/** Get the Tardis' user friendly name. Usually received by the {@linkplain TardisWorldCapability}*/
	String getTardisName(ItemStack stack);

	void setTardisName(ItemStack stack, String name);
}