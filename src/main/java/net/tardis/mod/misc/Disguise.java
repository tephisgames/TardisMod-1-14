package net.tardis.mod.misc;

import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Supplier;

import org.apache.logging.log4j.Level;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.mojang.serialization.Codec;
import com.mojang.serialization.JsonOps;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import net.minecraft.block.BlockState;
import net.minecraft.resources.IResource;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.registries.ForgeRegistryEntry;
import net.tardis.mod.Tardis;

public class Disguise extends ForgeRegistryEntry<Disguise> {

    BiFunction<World, BlockPos, Boolean> isValid = (world, pos) -> false;
    private Supplier<BlockState> top;
    private Supplier<BlockState> bottom;
    private HashMap<BlockPos, BlockState> blocks = new HashMap<BlockPos, BlockState>();
    private int width, height;


    public Disguise(Supplier<BlockState> top, Supplier<BlockState> bottom) {
        this.top = top;
        this.bottom = bottom;
    }

    public Disguise(Supplier<BlockState> both) {
        this(both, both);
    }

    public void readData(MinecraftServer server, ResourceLocation registryName) {
        try {
            this.blocks.clear();
            IResource resource = server.getDataPackRegistries().getResourceManager().getResource(new ResourceLocation(registryName.getNamespace(), "disguises/" + registryName.getPath() + ".json"));
            JsonArray root = new JsonParser().parse(new InputStreamReader(resource.getInputStream())).getAsJsonArray();
            for (JsonElement ele : root) {
                JsonObject obj = ele.getAsJsonObject();
                DisguiseBlockStateEntry.CODEC.decode(JsonOps.INSTANCE, obj)
                        .get()
        				.ifLeft(result -> {
        					DisguiseBlockStateEntry entry = result.getFirst();
        					BlockState state = entry.getBlockState();
        					int x = entry.getOffset().getX();
            	            int y = entry.getOffset().getY();
            	            int z = entry.getOffset().getZ();
            	            this.blocks.put(new BlockPos(x, y, z).toImmutable(), state);
        				})
        				.ifRight(partial -> Tardis.LOGGER.error("Failed to parse data json for {} due to: {}", registryName.toString(), partial.message()));   
            }
            resource.close();

        } catch (Exception e) {
            Tardis.LOGGER.catching(Level.DEBUG, e);
        }

        //set height and width
        for (BlockPos pos : this.blocks.keySet()) {
            if (Math.abs(pos.getX()) > this.width)
                this.width = Math.abs(pos.getX());
            if (Math.abs(pos.getZ()) > this.width)
                this.width = Math.abs(pos.getZ());

            if (pos.getY() > this.height)
                this.height = pos.getY();
        }

    }

    public void setValidationFunction(BiFunction<World, BlockPos, Boolean> func) {
        this.isValid = func;
    }

    public BlockState getTopState() {
        return top.get();
    }

    public BlockState getBottomState() {
        return this.bottom.get();
    }

    public HashMap<BlockPos, BlockState> getOtherBlocks() {
        return this.blocks;
    }

    public boolean getIsValid(World world, BlockPos pos) {
        return isValid.apply(world, pos);
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }
    
    /** Wrapper class that allows us to deserialise and serialise the blocks used in the disguise, incase we want to view the disguise without using a Tardis*/
    public static class DisguiseBlockStateEntry {
    	
    	public static final Codec<DisguiseBlockStateEntry> CODEC = RecordCodecBuilder.create(instance -> {
    		return instance.group(
    				BlockState.CODEC.fieldOf("block").forGetter(DisguiseBlockStateEntry::getBlockState),
    				Codec.INT.fieldOf("x").forGetter(offset -> offset.getOffset().getX()),
    				Codec.INT.fieldOf("y").forGetter(offset -> offset.getOffset().getY()),
    				Codec.INT.fieldOf("z").forGetter(offset -> offset.getOffset().getZ())
    				).apply(instance, DisguiseBlockStateEntry::new);
    	});
    	
    	public static final Codec<List<DisguiseBlockStateEntry>> CODEC_LIST = DisguiseBlockStateEntry.CODEC.listOf();
    	
    	private BlockPos offset;
    	private BlockState blockState;
    	
    	public DisguiseBlockStateEntry(BlockState state, BlockPos offset) {
    		this.blockState = state;
    		this.offset = offset;
    	}
    	
    	public DisguiseBlockStateEntry(BlockState state, int x, int y, int z) {
    		this.blockState = state;
    		this.offset = new BlockPos(x,y,z);
    	}
    	
    	public BlockPos getOffset() {
    		return this.offset;
    	}
    	
    	public BlockState getBlockState() {
    		return this.blockState;
    	}
    	
    	public static void deserialise() {
    		
    	}
    }
    
}
