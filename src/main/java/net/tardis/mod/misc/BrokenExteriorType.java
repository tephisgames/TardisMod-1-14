package net.tardis.mod.misc;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.function.Supplier;

import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.particles.ParticleTypes;
import net.minecraft.util.Direction;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundCategory;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerWorld;
import net.tardis.mod.ars.ConsoleRoom;
import net.tardis.mod.blocks.ConsoleBlock;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.exterior.AbstractExterior;
import net.tardis.mod.helper.TardisHelper;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.BrokenExteriorTile;
import net.tardis.mod.tileentities.console.misc.EmotionHandler;

public class BrokenExteriorType {

    Supplier<AbstractExterior> exterior;
    Supplier<ConsoleRoom> interior;
    Supplier<Console> consoleBlock;
    Supplier<ConsoleRoom[]> unlockedRooms;
	/**
	 * 
	 * @param exterior
	 * @param consoleBlock
	 * @param room
	 * @param unlockedRooms - additional rooms to unlock for this Tardis after taming. Do NOT add abandoned interiors to the unlock
	 */
    public BrokenExteriorType(Supplier<AbstractExterior> exterior, Supplier<Console> consoleBlock, Supplier<ConsoleRoom> room, Supplier<ConsoleRoom[]> unlockedRooms) {
        this.exterior = exterior;
        this.interior = room;
        this.consoleBlock = consoleBlock;
        this.unlockedRooms = unlockedRooms;
    }

    /**
     * @param world - world that contains the broken exterior
     * @param pos   - postion that the broken exterior is at
     * @param interior  - the tardis interior dimension
     * @param dir - the direction the exterior should be facing when spawned in 
     */
    public void swapWithReal(ServerWorld world, BlockPos pos, World interior, Direction dir) {

        TardisHelper.getConsole(world.getServer(), interior).ifPresent(tile -> {

            tile.setExteriorType(this.exterior.get());
            tile.getUnlockManager().addExterior(this.exterior.get());
            tile.getUnlockManager().addConsole(this.consoleBlock.get());
            tile.setCurrentLocation(world.getDimensionKey(), pos.down());
            tile.setDestination(world.getDimensionKey(), pos.down());
            tile.setExteriorFacingDirection(dir);
            tile.getExteriorType().place(tile, world.getDimensionKey(), pos.down());
            tile.getExteriorType().getExteriorTile(tile).setInteriorDimensionKey(interior);
            tile.getExteriorType().getExteriorTile(tile).setDoorState(EnumDoorState.ONE);
        });
    }

    /**
     * @param world
     * @param player
     * @return - true if setup, if not it will warn the player of the reason on it's own
     */
    public ServerWorld setup(ServerWorld world, ServerPlayerEntity player, BrokenExteriorTile ext) {
        ServerWorld tardisWorld = TardisHelper.setupTardisDim(world.getServer(), this.consoleBlock.get().getState(), this.interior.get());
        world.playSound(null, ext.getPos(), TSounds.DOOR_UNLOCK.get(), SoundCategory.BLOCKS, 0.75F, 1F); //Play an audio cue
        
        //Unlock room
        TardisHelper.getConsoleInWorld(tardisWorld).ifPresent(tile -> {
            ConsoleRoom[] rooms = this.unlockedRooms.get();
            for(ConsoleRoom room : rooms) {
                if(room != null) {
                    tile.getUnlockManager().addConsoleRoom(room);
                }
            }
            tile.getUnlockManager().addConsole(this.consoleBlock.get());
            if (this.interior.get() != null) { //Add missing logic
            	tile.setConsoleRoom(this.interior.get()); //Set the current console room to that of the supplied interior
            }
            HashMap<UUID, Integer> extLoyalties = ext.getLoyalties();
            EmotionHandler emotionHandler = tile.getEmotionHandler();

            /* Copy across the loyalties (or lack thereof) that the exterior had built up
            There was an issue in dev builds where we thought that this code block was causing initial loyalty to be too high.
            This is not the case, it was caused by something in BrokenExteriorTile.
            We simply just didn't port over that initial loyalty value logic from 1.14 to the ownerless system in 1.16.
            */
            for(Map.Entry<UUID, Integer> entry : extLoyalties.entrySet()) {
                UUID key = entry.getKey();
                int value = entry.getValue();

                emotionHandler.addLoyalty(key,value);
            }
            
        });
        
        return tardisWorld;
    }

    /**
     * Spawns the heart effect
     *
     * @param world
     * @param pos
     * @param rand
     */
    public void spawnHappyPart(World world, BlockPos pos) {
        for (int i = 0; i < 18; ++i) {
            double angle = Math.toRadians(i * 20);
            double x = Math.sin(angle);
            double z = Math.cos(angle);

            world.addParticle(ParticleTypes.HEART, pos.getX() + 0.5 + x, pos.getY() + world.getRandom().nextDouble(), pos.getZ() + 0.5 + z, 0, 0, 0);
        }
    }

}
