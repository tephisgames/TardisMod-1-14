package net.tardis.mod.misc;

import com.google.gson.JsonElement;
import com.google.gson.JsonSyntaxException;
import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.Dynamic;
import com.mojang.serialization.JsonOps;

import net.tardis.mod.schematics.Schematic;

public class SchematicCodec {
	
	public static final Codec<Schematic> SCHEMATIC_TO_JSON_CODEC = Codec.PASSTHROUGH.comapFlatMap(
	        json -> DataResult.error("Deserializing of schematic not implemented"),
	        schematic -> new Dynamic<JsonElement>(JsonOps.INSTANCE, schematic.serialise()));
    
	public static final Codec<Schematic> SCHEMATIC_FROM_JSON_CODEC = Codec.PASSTHROUGH.flatXmap(dynamic ->
	{
	    try
	    {
	    	Schematic schematic = Schematic.deserialise(dynamic.convert(JsonOps.INSTANCE).getValue());
	        return DataResult.success(schematic);
	    }
	    catch(JsonSyntaxException e)
	    {
	        return DataResult.error(e.getMessage());
	    }
	},
	schematic -> DataResult.error("Cannot serialize schematic to json with this Codec!\n Use another codec for this"));
	
	public static final Codec<Schematic> SCHEMATIC_CODEC = Codec.of(SCHEMATIC_TO_JSON_CODEC, SCHEMATIC_FROM_JSON_CODEC);
}
