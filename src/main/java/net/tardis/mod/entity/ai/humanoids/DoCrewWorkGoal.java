package net.tardis.mod.entity.ai.humanoids;

import net.minecraft.block.BlockState;
import net.minecraft.entity.CreatureEntity;
import net.minecraft.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.state.properties.BlockStateProperties;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldReader;
import net.tardis.mod.tags.TardisBlockTags;

public class DoCrewWorkGoal extends MoveToBlockGoal{
	
	public DoCrewWorkGoal(CreatureEntity creature, double speedIn, int length) {
		super(creature, speedIn, length, 1);
	}

	@Override
	protected boolean shouldMoveTo(IWorldReader worldIn, BlockPos pos) {
		return worldIn.getBlockState(pos).isIn(TardisBlockTags.CREW_TASK);
	}

	@Override
	public boolean shouldContinueExecuting() {
		return super.shouldContinueExecuting();
	}

	@Override
	protected boolean searchForDestination() {
		boolean success = super.searchForDestination();
		
		if(success) {
			BlockState state = this.creature.world.getBlockState(this.destinationBlock);
			if(state.hasProperty(BlockStateProperties.HORIZONTAL_FACING))
				this.destinationBlock = this.destinationBlock.offset(state.get(BlockStateProperties.HORIZONTAL_FACING)).down();
		}
		
		return success;
	}

	@Override
	public void tick() {
		super.tick();
		if(this.getIsAboveDestination()){
			if(this.creature.ticksExisted % 20 == 0) {
				this.creature.swingArm(Hand.MAIN_HAND);
			}
		}
	}

}
