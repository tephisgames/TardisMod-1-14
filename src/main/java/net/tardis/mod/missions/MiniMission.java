package net.tardis.mod.missions;

import java.util.Collections;
import java.util.List;

import javax.annotation.Nullable;

import com.google.common.collect.Lists;

import net.minecraft.entity.Entity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.Util;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TranslationTextComponent;
import net.minecraft.world.BossInfo;
import net.minecraft.world.World;
import net.minecraft.world.server.ServerBossInfo;
import net.minecraft.world.server.ServerWorld;
import net.minecraftforge.common.util.INBTSerializable;
import net.tardis.mod.Tardis;
import net.tardis.mod.missions.misc.Dialog;
import net.tardis.mod.missions.misc.DialogOption;
import net.tardis.mod.network.Network;
import net.tardis.mod.network.packets.MissionUpdateMessage;

public abstract class MiniMission implements INBTSerializable<CompoundNBT> {

    private World world;
    private BlockPos pos;
    private int range;
    private int stage;
    private boolean awarded = false;
    private MiniMissionType type;
    private TranslationTextComponent barName;
    private ServerBossInfo bossBar;
    private List<PlayerEntity> trackingPlayers = Lists.newArrayList();
	private List<Dialog> dialogs = Lists.newArrayList(); //Used to allow for language generators to generate lang file contents easily
	private List<DialogOption> dialogOptions = Lists.newArrayList();
    private AxisAlignedBB missionBox = null;

    public MiniMission(MiniMissionType type, World world, BlockPos pos, int range) {
        this.world = world;
        this.pos = pos;
        this.range = range;
        this.type = type;
        this.barName = new TranslationTextComponent(Util.makeTranslationKey("missions", type.getRegistryName()));
        this.bossBar = new ServerBossInfo(this.getMissionName(), BossInfo.Color.WHITE, BossInfo.Overlay.PROGRESS);
        this.bossBar.setPercent(0);
    }

    public MiniMission(MiniMissionType type, World world, CompoundNBT tag) {
        this(type, world, BlockPos.ZERO, 0);
        this.deserializeNBT(tag);
    }

    public abstract ItemStack getReward();

    public abstract int getMaxStage();

    public abstract float getProgressBarPercent();

    public abstract Dialog getDialogForStage(LivingEntity speaker, PlayerEntity player, int stage);

    public TranslationTextComponent getMissionName() {
        return this.barName;
    }

    public int getStage() {
        return this.stage;
    }

    public void setStage(int stage) {
        this.stage = stage;
        if (this.bossBar != null)
            this.getBossBar().setPercent(this.getProgressBarPercent());
        this.update();
    }

    public void advanceStage() {
        this.setStage(this.stage + 1);
    }

    public int getRange() {
        return this.range;
    }

    public BlockPos getPos() {
        return this.pos;
    }

    public World getWorld() {
        return this.world;
    }

    public void setAwarded(boolean awarded) {
        this.awarded = awarded;
        this.update();
    }

    public boolean getAwarded() {
        return this.awarded;
    }

    public boolean isComplete() {
        return this.stage >= this.getMaxStage();
    }

    public void onPlayerEnterMissionArea(PlayerEntity player) {
        if (player instanceof ServerPlayerEntity) {
            this.bossBar.addPlayer((ServerPlayerEntity) player);
        }
    }

    public void onPlayerLeaveMissionArea(PlayerEntity player) {
        if (player instanceof ServerPlayerEntity) {
            this.bossBar.removePlayer((ServerPlayerEntity) player);
        }
    }

    @Override
    public CompoundNBT serializeNBT() {
        CompoundNBT tag = new CompoundNBT();

        tag.putLong("pos", this.pos.toLong());
        tag.putInt("range", this.range);
        tag.putInt("stage", this.stage);
        tag.putBoolean("awarded", this.awarded);

        return tag;
    }

    @Override
    public void deserializeNBT(CompoundNBT tag) {
        this.pos = BlockPos.fromLong(tag.getLong("pos"));
        this.range = tag.getInt("range");
        this.stage = tag.getInt("stage");
        this.awarded = tag.getBoolean("awarded");
    }

    public MiniMissionType getType() {
        return this.type;
    }

    public ServerBossInfo getBossBar() {
        return this.bossBar;
    }

    public boolean isInsideArea(Entity ent) {
    	if(world.getDimensionKey() != ent.world.getDimensionKey())
			return false;
        return this.pos.withinDistance(ent.getPosition(), range);
    }

    public List<PlayerEntity> getTrackingPlayers() {
        return Collections.unmodifiableList(this.trackingPlayers);
    }

    public void addTrackingPlayer(ServerPlayerEntity player) {
        if (!this.trackingPlayers.contains(player)) {
            this.trackingPlayers.add(player);
            this.bossBar.addPlayer(player);
        }
    }

    public void removeTrackingPlayer(ServerPlayerEntity player) {
        if (this.trackingPlayers.contains(player)) {
            this.trackingPlayers.remove(player);
            this.bossBar.removePlayer(player);
        }
    }

    public AxisAlignedBB getMissionBB() {
        if (this.missionBox == null)
            this.missionBox = new AxisAlignedBB(this.pos).grow(this.range);
        return this.missionBox;
    }

    public void update() {
        if (!world.isRemote)
            Network.sendToAllInWorld(new MissionUpdateMessage(this), (ServerWorld) world);
    }

	//========= Character Dialog Helpers ===========
    /** Helper methods to make dialog creation easy */
    public Dialog createDialogForCharacter(String translationSuffixKey) {
        Dialog dialog = new Dialog(createDialogTranslationKey(translationSuffixKey));
        dialogs.add(dialog);
        return dialog;
    }
    
    public String createDialogTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog.character." + translationSuffixKey;
    }

    public TranslationTextComponent createDialogTranslationKey(String modId, String translationSuffixKey) {
        return new TranslationTextComponent("mission." + modId + "." + this.getType().getRegistryName().getPath().toString() +  ".dialog.character." + translationSuffixKey);
    }
    
    
	
    //========= Player Dialog Option Helpers =========
    
    public DialogOption createPlayerResponse(@Nullable Dialog dialog, TranslationTextComponent responseKey) {
        return new DialogOption(dialog, responseKey);
    }
    
    /**
     * @param dialog
     * @param response - the translation key that will be appended to the default translation key
     * @return
     */
    public DialogOption createPlayerResponse(@Nullable Dialog dialog, String responseKey) {
        DialogOption dialogOption = new DialogOption(dialog, new TranslationTextComponent(createPlayerResponseTranslationKey(responseKey)));
        dialogOptions.add(dialogOption);
        return dialogOption;
    }
    
    public DialogOption createPlayerResponseWithArguments(@Nullable Dialog dialog, String responseKey, String args) {
        DialogOption dialogOption = new DialogOption(dialog, new TranslationTextComponent(createPlayerResponseTranslationKey(responseKey), args));
        dialogOptions.add(dialogOption);
        return dialogOption;
    }
	
	/** Dialog Option Helpers*/
	
	
    public String createPlayerResponseTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player." + translationSuffixKey;
    }
    
    
    public String createDialogOptionTranslationKey(String translationSuffixKey) {
        return "mission." + Tardis.MODID + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player" + translationSuffixKey;
    }
    
    public TranslationTextComponent createDialogOptionTranslationKey(String modId, String translationSuffixKey) {
        return new TranslationTextComponent("mission." + modId + "." + this.getType().getRegistryName().getPath().toString() + ".dialog_option.player" + translationSuffixKey);
    }
}
