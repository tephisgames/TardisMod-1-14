package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.blocks.ExteriorBlock;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.entity.DoorEntity;
import net.tardis.mod.enums.EnumDoorState;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class DoorControl extends BaseControl{

	public DoorControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.125F, 0.125F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.0625F, 0.0625F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(0.1625F, 0.1625F);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		boolean lock = player.isSneaking();
		if(!console.getWorld().isRemote()) {
			for(DoorEntity ent : console.getWorld().getEntitiesWithinAABB(DoorEntity.class, new AxisAlignedBB(console.getPos()).grow(25))) {
				if(lock) {
					if (ent.isDeadLocked()) {
						player.sendStatusMessage(ExteriorBlock.DEADLOCKED, true);
					}
					else {
						ent.setLocked(!ent.isLocked());
						ent.playSound(ent.isLocked() ? TSounds.DOOR_LOCK.get() : TSounds.DOOR_UNLOCK.get(), 1F, 1F);
						player.sendStatusMessage(ent.isLocked() ? ExteriorBlock.LOCKED : ExteriorBlock.UNLOCKED, true);
						ent.setOpenState(EnumDoorState.CLOSED);	
					}
				}
				if(!ent.isDeadLocked()){
					if (!ent.isLocked()) {
						if(ent.getOpenState() == EnumDoorState.CLOSED)
							ent.setOpenState(EnumDoorState.BOTH);
						else ent.setOpenState(EnumDoorState.CLOSED);
						ent.playSound(ent.getOpenState() == EnumDoorState.CLOSED ? TSounds.DOOR_CLOSE.get() : TSounds.DOOR_OPEN.get(), 1F, 1F);
						this.startAnimation();
					}
					else player.sendStatusMessage(ExteriorBlock.LOCKED, true);
				}
				else {
					player.sendStatusMessage(ExteriorBlock.DEADLOCKED, true);
				}
				ent.updateExteriorDoorData();
			}
		}
		return true;
	}

	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(0.6213499136129208, 0.42499999701976776, -0.36428324173310445);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(0.7811600989048273, 0.46875, -0.1477678772167177);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(0.6762866682671369, 0.34375, -0.6);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(0.258, 0.438, -0.806);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.217, 0.469, -0.81);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.3754074885766546, 0.40625, 0.7);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-0.7584815683766266, 0.40625, 0.7779226605452545);

		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.6969155751590517, 0.40625, -0.7690664736409507);
		
		return new Vector3d(-0.7238774917092281, 0.5, 0.8087972034724291);
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return null;
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_TWO.get();
	}

	@Override
	public CompoundNBT serializeNBT() {
		return new CompoundNBT();
	}

	@Override
	public void deserializeNBT(CompoundNBT nbt) {}

}
