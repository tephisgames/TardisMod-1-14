package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.minecraft.util.text.StringTextComponent;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.util.text.TranslationTextComponent;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class IncModControl extends BaseControl{
	
	public static final String USE_TRANSLATION = "message." + Tardis.MODID + ".control.inc_mod";
	public static int[] COORD_MODS = new int[] {1, 10, 100, 1000};
	public int index;
	
	public IncModControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}

	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(4 / 16.0F,  4 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1625F, 0.1625F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof XionConsoleTile)
		    return EntitySize.flexible(0.125F, 0.125F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.125F, 0.125F);
		
		return EntitySize.flexible(3 / 16.0F, 2 / 16F);
	}
	
	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return new Vector3d(-13 / 16.0, 6 / 16.0F, 7 / 16.0F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.6071484912786207, 0.39374999701976776, 0.3642372909987306);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.5, 0.5625, 0.3175);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.59, 0.469, 0.092);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.503, 0.469, 0.525);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(0.5333379834907053, 0.34375, 0.9640984733364331);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(0.8195188587203308, 0.375, 0.47455568332207365);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.6633852686396295, 0.34375, -0.9062030791378303);
		
		return new Vector3d(-11 / 16.0, 7 / 16.0, -11 / 16.0);
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(!console.getWorld().isRemote()) {
			if(player.isSneaking()) {
				if(index - 1 < 0)
					index = COORD_MODS.length - 1;
				else index -= 1;
				console.coordIncr = COORD_MODS[index];
			}
			else {
				if(index + 1 >= COORD_MODS.length)
					index = 0;
				else index += 1;
				console.coordIncr = COORD_MODS[index];
				console.updateClient();
			}
			this.startAnimation();
			player.sendStatusMessage(new TranslationTextComponent(USE_TRANSLATION).appendSibling(new StringTextComponent(String.valueOf(console.coordIncr)).mergeStyle(TextFormatting.LIGHT_PURPLE)), true);
		}
		return true;
	}

	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return TSounds.GENERIC_TWO.get();
	}

	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.index = tag.getInt("index");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putInt("index", index);
		return tag;
	}

}
