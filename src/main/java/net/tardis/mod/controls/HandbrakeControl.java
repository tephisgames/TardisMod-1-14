package net.tardis.mod.controls;

import net.minecraft.entity.EntitySize;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.CompoundNBT;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvent;
import net.minecraft.util.math.vector.Vector3d;
import net.tardis.mod.Tardis;
import net.tardis.mod.entity.ControlEntity;
import net.tardis.mod.misc.CrashType;
import net.tardis.mod.registries.ControlRegistry.ControlEntry;
import net.tardis.mod.sounds.TSounds;
import net.tardis.mod.tileentities.ConsoleTile;
import net.tardis.mod.tileentities.consoles.CoralConsoleTile;
import net.tardis.mod.tileentities.consoles.GalvanicConsoleTile;
import net.tardis.mod.tileentities.consoles.HartnellConsoleTile;
import net.tardis.mod.tileentities.consoles.KeltConsoleTile;
import net.tardis.mod.tileentities.consoles.NemoConsoleTile;
import net.tardis.mod.tileentities.consoles.NeutronConsoleTile;
import net.tardis.mod.tileentities.consoles.ToyotaConsoleTile;
import net.tardis.mod.tileentities.consoles.XionConsoleTile;

public class HandbrakeControl extends BaseControl {

	public static final ResourceLocation SAVE_KEY = new ResourceLocation(Tardis.MODID, "handbrake_data");
	/*
	 * True if brake is off and TARDIS can fly
	 */
	private boolean isFree = false;

	public HandbrakeControl(ControlEntry entry, ConsoleTile console, ControlEntity entity) {
		super(entry, console, entity);
	}
	
	@Override
	public EntitySize getSize() {
		if(this.getConsole() instanceof NemoConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof GalvanicConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		if(getConsole() instanceof CoralConsoleTile){
			return EntitySize.flexible(0.1875F, 0.1875F);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if (this.getConsole() instanceof XionConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return EntitySize.flexible(0.25F, 0.25F);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return EntitySize.flexible(0.1875F, 0.1875F);

		return EntitySize.flexible(0.1875F, 0.1875F);
	}
	
	@Override
	public Vector3d getPos() {
		if(this.getConsole() instanceof NemoConsoleTile) 
			return new Vector3d(-2 / 16.0, 8 / 16.0, -13 / 16.0F);

		if(this.getConsole() instanceof GalvanicConsoleTile)
			return new Vector3d(-0.2982304929003854, 0.375, 0.8465142260574359);

		if(getConsole() instanceof CoralConsoleTile){
			return new Vector3d(-0.7203601993321677, 0.5625, -0.3625);
		}
		
		if(this.getConsole() instanceof HartnellConsoleTile)
			return new Vector3d(-0.059, 0.375, 0.774);
		
		if(this.getConsole() instanceof ToyotaConsoleTile)
			return new Vector3d(-0.206, 0.375, 0.768);
		
		if(this.getConsole() instanceof XionConsoleTile)
			return new Vector3d(-1.127392157437948, 0.1875, -0.0012375564178497278);
		
		if(this.getConsole() instanceof NeutronConsoleTile)
			return new Vector3d(-1.1546022251848151, 0.59375, -0.00377360907123081);
		
		if(this.getConsole() instanceof KeltConsoleTile)
			return new Vector3d(0.4691775280536088, 0.375, 1.0251364745327698);
		
		return new Vector3d(0.31798977635472236, 0.48749999701976776, 0.9024203281819716);
	}

	
	@Override
	public void deserializeNBT(CompoundNBT tag) {
		this.isFree = tag.getBoolean("free");
	}

	@Override
	public CompoundNBT serializeNBT() {
		CompoundNBT tag = new CompoundNBT();
		tag.putBoolean("free", this.isFree);
		return tag;
	}

	@Override
	public boolean onRightClicked(ConsoleTile console, PlayerEntity player) {
		if(console == null || !console.hasWorld())
			return false;
		if(!console.getWorld().isRemote()) {
			this.isFree = !this.isFree;
			
			//Crash
			if(console.isInFlight() && !this.isFree)
				console.crash(CrashType.DEFAULT);
			
			console.getControl(ThrottleControl.class).ifPresent(throttle -> {
				if(throttle.getAmount() > 0.0F) {
					if(this.isFree()) {
						console.takeoff();
					}
				}
			});
			
			
			this.markDirty();
		}
		return true;
	}
	
	@Override
	public SoundEvent getFailSound(ConsoleTile console) {
		return TSounds.SINGLE_CLOISTER.get();
	}

	@Override
	public SoundEvent getSuccessSound(ConsoleTile console) {
		return isFree() ? TSounds.HANDBRAKE_RELEASE.get() : TSounds.HANDBRAKE_ENGAGE.get();
	}

	public boolean isFree() {
		return this.isFree;
	}
	
	public void setFree(boolean free) {
		this.isFree = free;
		this.markDirty();
	}
	
	@Override
	public ResourceLocation getAdditionalDataSaveKey() {
		return SAVE_KEY;
	}

	@Override
	public void setConsole(ConsoleTile console, ControlEntity entity) {
		super.setConsole(console, entity);
		console.registerDataHandler(SAVE_KEY, this);
	}

}
