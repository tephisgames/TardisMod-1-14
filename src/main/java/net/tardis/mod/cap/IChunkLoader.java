package net.tardis.mod.cap;

import net.minecraft.util.math.BlockPos;

/**
 * Created by 50ap5ud5
 * on 7 Mar 2020 @ 9:53:51 am
 */
public interface IChunkLoader {
    void add(BlockPos pos);

    void remove(BlockPos pos);

    boolean contains(BlockPos pos);
}
